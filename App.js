/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    AppRegistry,
    BackHandler,
    BackAndroid, AppState
} from 'react-native';
import {
    createStore,
    applyMiddleware,
    combineReducers,
} from 'redux';
import { Provider,connect } from 'react-redux';
import {addNavigationHelpers,NavigationActions} from 'react-navigation';
import store from './app/store'
import {
    createReduxBoundAddListener,
    createReactNavigationReduxMiddleware,
} from 'react-navigation-redux-helpers';
import nav from './app/store/reducers/nav';
import Splash from './app/login/Home';
import Navigator from './app/login/Routes';
import SignUp from './app/login/SignUp';
import ForgotPassword from "./app/login/ForgotPassword";
import Main from './app/login/Routes';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});



function shouldCloseApp(nav){
    return nav.index == 0;
}

const rootReducer = combineReducers({
    nav: nav,
});



const middleware = createReactNavigationReduxMiddleware(
    "root",
    state => state.nav,
);


const addListener = createReduxBoundAddListener("root");

class VoiseApp extends Component{
    componentWillMount(){
        debugger
        AppState.addEventListener('change', (state) => {
            if (state === 'active') {
                console.log('state active');
            }
            if(state === 'background'){
                console.log('background');
            }
        });
    }
    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress = () => {
        debugger
        const { dispatch, nav,navigation } = this.props;
        if(nav.routes[1]!==undefined && nav.routes[1].isTransitioning===true){
            BackHandler.exitApp();
        }
        if(nav.routes[1]!==undefined && nav.routes[1].index===0){
            BackHandler.exitApp();
        }
        if (nav.index === 0) {

            return false;
        }
        dispatch(NavigationActions.back());
        return true;
    };
    render(){
        return(
            <Navigator navigation={addNavigationHelpers({dispatch:this.props.dispatch,
                state:this.props.nav,addListener})}/>
        );
    }

}
const mapStateToProps=(state)=>{
    return{
        nav:state.nav,
    }
}

const AppwithNavigation=connect(mapStateToProps)(VoiseApp);


export default class App extends Component{

  render() {
    return (
        <Provider store={store}>

            <AppwithNavigation navigation={addNavigationHelpers({ dispatch: this.props.dispatch, state: this.props.nav })}/>
        </Provider>





    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems:'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

// AppRegistry.registerComponent('voise',()=>Navigator);
