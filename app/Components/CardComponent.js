import React, {Component} from 'react';
import {View,Text,Image,TouchableOpacity,Dimensions,ActivityIndicator,StyleSheet} from 'react-native';
import {Card,Icon} from 'react-native-elements';
import PPopover from './Popover';
import imageCacheHoc from 'react-native-image-cache-hoc';




const styles = StyleSheet.create({
    activityIndicatorStyle: {
        width: 130,
        height: 160,
        backgroundColor: '#000'
    }
});








const defaultPlaceholderObject = {
    component: ActivityIndicator ,
    props:{
        style:styles.activityIndicatorStyle
    }

};



const CacheableImage = imageCacheHoc(Image, {
    defaultPlaceholder: defaultPlaceholderObject,
    validProtocols: ['http', 'https']
});
const { Width } = Dimensions.get('window').width;
export default class CardComponent extends  Component{
    constructor(props){
        super(props);
    }

   componentWillReceiveProps(nextProps){

   }
   componentWillMount(){


   }
   componentDidMount(){

   }
    render(){


        return(
            <View style={{maxWidth:this.props.genre && Dimensions.get('window').width}} >




                   <Card containerStyle={{flex:1,padding: 0, margin: this.props.genre?0:1,
                       height: this.props.genre?100:160,
                       width: this.props.genre?Dimensions.get('window').width/2:130,
                       maxWidth:(Width/2),
                       marginRight: this.props.genre?0:4,
                       borderColor: '#000', backgroundColor:'transparent'}}  wrapperStyle={{backgroundColor:'transparent'}}>
                       <TouchableOpacity key={this.props.id} id={this.props.id} onPress={() => {
                           this.props.icon?this.props.onPlay({id:this.props.id,url:this.props.url,imageUrl:this.props.Imgurl,songName:this.props.Text}):this.props.onCardPress({id:this.props.id,url:this.props.url})}}>
                     <CacheableImage source={{uri:this.props.Imgurl}} style={this.props.imagestyle} permanent={false}/>
                    </TouchableOpacity>
                       {this.props.icon?<TouchableOpacity onPress={() => this.props.popover(this.props.ids,this.props.Imgurl,this.props.Text,this.props.Artist)}>
                       <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                          <View style={{width:'80%',backgroundColor:'transparent'}}>
                           <Text numberOfLines={1} style={{fontSize:12,color:'#fff',alignSelf:'flex-start',marginTop:4,marginLeft:4}}>{this.props.Text}</Text>


                          </View>
                           <View>
                               <Icon size={14} iconStyle={{color:'white',alignSelf: 'flex-end', marginRight: 4, marginTop: 6}}
                                     name={'ellipsis-v'} type='font-awesome'/>
                           </View>

                           </View>
                       <Text style={{fontSize:8,color:'#fff',marginLeft:4}}>
                           {this.props.Artist&&this.props.Artist}
                       </Text>
                       <Text style={{fontSize:8,color:'#fff'}}>
                           {this.props.duration&&this.props.duration}
                       </Text>
                       </TouchableOpacity>:
                           <View style={{backgroundColor:'transparent'}}>
                           <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                               <View style={{width:'80%',backgroundColor:'transparent'}}>
                                   <Text numberOfLines={1} style={{fontSize:12,color:'#fff',alignSelf:'flex-start',marginTop:4,marginLeft:4}}>{this.props.Text}</Text>
                               </View>
                           </View>
                           <Text style={{fontSize:8,color:'#fff',marginLeft:4}}>
                               {this.props.Artist&&this.props.Artist}
                           </Text>
                           <Text style={{fontSize:8,color:'#fff'}}>
                       {this.props.duration&&this.props.duration}
                           </Text>
                           </View> }

                   </Card>


            </View>

        );
    }
}
