import React,{Component} from 'React';
import { DrawerNavigator, SafeAreaView } from 'react-navigation';
import Dashboard from '../login/screens/DashBoard';
import HeaderComponent from './HeaderComponent';

export default DrawerNavigator({
            Dashboard: {
                path: '/',
                screen: Dashboard,
            },

        },
    {
        drawerOpenRoute: 'DrawerOpen',
        drawerCloseRoute: 'DrawerClose',
        drawerToggleRoute: 'DrawerToggle',
        initialRouteName: 'Drafts',
        contentOptions: {
            activeTintColor: '#e91e63',
        },
    }
    );



