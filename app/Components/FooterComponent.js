import React, { Component } from 'react';
import {Text,StyleSheet,ToastAndroid,View,Animated,PanResponder} from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button,Toast } from 'native-base';
import {Icon} from 'react-native-elements';
import {NavigationActions} from 'react-navigation';
import PlayerComponent from './PlayerComponent';
import {connect} from 'react-redux';
import _ from 'lodash';
import genreContainer from "../Containers/genreContainer";
 class FooterComponent extends Component {
    constructor(props){
        super(props);
        this.state={
            currentTab:1,
            showToast: false,
            player:false,
            pan: new Animated.ValueXY()

        }
        this.setActive=this.setActive.bind(this);

    }
    componentWillMount(){

        const touchThreshold = 20;
        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => false,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => false,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponder : (e, gestureState) => {
                const {dx, dy} = gestureState;

                return (Math.abs(dx) > touchThreshold) || (Math.abs(dy) > touchThreshold);
            },
            onPanResponderGrant: () => this.setState({scroll: false}),
            onPanResponderMove: Animated.event([null, {dx: this.state.pan.x, dy: this.state.pan.y}]),
            onPanResponderRelease:  (e, gesture)  => {
                Animated.spring(this.state.pan, {
                    toValue: { x: 0, y: 0 },

                }).start()
                this.props.navigation.navigate('Player');
            }

        })
    }
    componentDidMount(){

        let index=this.props.navigation.state.index;
        this.setActives(1);
        if(index+1!==this.state.currentTab){
            this.setActives(1);
        }
    }
    componentDidUpdate(){

    }
     setActives(tabNo){


             this.setState({
                 currentTab: tabNo


             });

             if (tabNo == 1) {
                 if(this.props.navigation.state.index!==0) {
                     // this.props.navigation.dispatch(NavigationActions.reset({
                     //     index: 0, key: null, actions: [NavigationActions.navigate({ routeName: 'genre' })]
                     // }));
                     const wallet = NavigationActions.navigate({
                         routeName: "Home"
                     });
                     this.props.navigation.dispatch(wallet);

                 }
             }
             if (tabNo == 3) {
                 if(this.props.navigation.state.index!==1) {
                     // this.props.navigation.dispatch(NavigationActions.reset({
                     //     index: 0, key: null, actions: [NavigationActions.navigate({ routeName: 'drawerStack' })]
                     // }));
                     const playlist = NavigationActions.navigate({
                         routeName: "MyPlaylist"
                     });
                     this.props.navigation.dispatch(playlist);

                 }
             }
             if (tabNo == 2) {
                 if(this.props.navigation.state.index!==2) {
                     const playlist =  this.props.navigation.dispatch(NavigationActions.reset({
                         index: 0, key: null, actions: [NavigationActions.navigate({ routeName: 'genre' })]
                     }));
                     // const playlist = NavigationActions.navigate({
                     //     routeName: "genre"
                     // });
                     // this.props.navigation.dispatch(playlist);
                 }


             }
             if (tabNo == 4) {
                 debugger

                 if(this.props.navigation.state.index!==3) {
                     const wallet = NavigationActions.navigate({
                         routeName: "Wallet"
                     });
                     this.props.navigation.dispatch(wallet);
                 }


         }
     }
    setActive(tabNo){
            this.setState({
                currentTab: tabNo


            })

            if (tabNo == 1 && this.state.currentTab!==1) {
                const resetAction = NavigationActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({ routeName: 'Home',

                        }),
                    ],
                    key: 'Home' // THIS LINE
                });
                this.props.navigation.dispatch(resetAction);
                // const wallet = NavigationActions.navigate({
                //     routeName: "Home"
                // });
                // this.props.navigation.dispatch(wallet);


            }
            if (tabNo == 3 && this.state.currentTab!==3) {
                const resetAction = NavigationActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({ routeName: 'MyPlaylist',

                        }),
                    ],
                    key: 'MyPlaylist' // THIS LINE
                })
                this.props.navigation.dispatch(resetAction);
                // const playlist = NavigationActions.navigate({
                //     routeName: "MyPlaylist"
                // });
                // this.props.navigation.dispatch(playlist);


            }
            if (tabNo == 2 && this.state.currentTab!==2) {
              debugger
                    //const { navigate } = this.props.navigation
                    const resetAction = NavigationActions.reset({
                        index: 0,
                        actions: [
                            NavigationActions.navigate({ routeName: 'genre',

                            }),
                        ],
                        key: 'genre' // THIS LINE
                    });
                    this.props.navigation.dispatch(resetAction);




            }
            if (tabNo == 4 && this.state.currentTab!==4) {

                   const wallet = NavigationActions.navigate({
                       routeName: "Wallet"
                   });
                   this.props.navigation.dispatch(wallet);
               }

            }


    componentWillReceiveProps(nextProps){
        if(!_.isEmpty(nextProps.currentPlayingAlbum)||nextProps.isPlaying===true){
            this.setState({player:true});

        }

    }
    componentDidUpdate(){

        if(this.props.navigation.state.index===3 &&this.state.currentTab!==4){
        this.setState({currentTab:4});
        }else if (this.props.navigation.state.index===2 &&this.state.currentTab!==3){
            this.setState({currentTab:3});
        } else if(this.props.navigation.state.index===1 &&this.state.currentTab!==2){
            this.setState({currentTab:2});
        } else if(this.props.navigation.state.index===0 &&this.state.currentTab!==1){
            this.setState({currentTab:1});
        }
    }
    showWallet = () => {
	    this.props.navigation.navigate('Wallet');
    }
    render() {
        return (

        <View style={{backgroundColor:'#242424'}}>
            {/*<View style={this.state.player?{height:60,backgroundColor:'#242424'}:{height: 0, opacity: 0}}>*/}
                {/*<PlayerComponent showWallet={this.showWallet}/>*/}
            {/*</View>*/}
            <View style={{height:50,backgroundColor:'#242424',marginHorizontal:'3.5%'}}>
            <Footer style={{justifyContent:'center'}}>

                    <FooterTab style={{backgroundColor:'#242424',paddingHorizontal:'-1%'}} tabBarUnderlineStyle={{ backgroundColor: '#f65857' }}>
                        <View style
                                  ={this.state.currentTab===1 && styles.border}>
                        <Button active={this.state.currentTab===1} style
                            ={this.state.currentTab===1 && styles.background} onPress={()=>{this.setActive(1)}}>
                            {/*<Icon color={'#fff'} name="home" />*/}
                            <Text style={{color:'white',fontFamily:'Ubuntu-R',fontSize:12}}>Home</Text>
                        </Button>
                        </View>
                        <View style
                                  ={this.state.currentTab===2 && styles.border}>
                        <Button active={this.state.currentTab===2} style={this.state.currentTab===2 && styles.background} onPress={()=>{this.setActive(2)}} >
                            {/*<Icon color={'#fff'} size={25} name="music" type="font-awesome" />*/}
                            <Text style={{color:'white',fontFamily:'Ubuntu-R',fontSize:12}}>Genre</Text>
                        </Button>
                        </View>
                        <View style
                                  ={this.state.currentTab===3 && styles.border}>
                        <Button active={this.state.currentTab===3} style={this.state.currentTab===3 && styles.background} onPress={()=>{this.setActive(3)}}>
                            {/*<Icon color={'#fff'} name="playlist-play" type="MaterialCommunityIcons" />*/}
                            <Text style={{color:'white',fontFamily:'Ubuntu-R',fontSize:12}}>My Playlist</Text>
                        </Button>
                        </View>
                        <View style
                                  ={this.state.currentTab===4 && styles.border}>
                        <Button  active={this.state.currentTab===4}
                                 style={this.state.currentTab===4 && styles.background}
                                 onPress={()=>{this.setActive(4)}}>
                            {/*<Icon color={'#fff'} name="account-balance-wallet" />*/}
                            <Text style={{color:'white',fontFamily:'Ubuntu-R',fontSize:12}}>Wallet</Text>
                        </Button>
                        </View>
                    </FooterTab>
                </Footer>
            </View>
        </View>

        );
    }
}

const styles=StyleSheet.create({
    IconsCol:{
        color:'#fff',
        borderBottomWidth:4,
        borderBottomColor:'#fff',


    },
    background:{
        // borderBottomWidth:4,
        // borderBottomColor:'#fff',
        backgroundColor:'transparent',
        borderBottomWidth:6,
        borderBottomColor:'#ec223f'

    },
    border:{
        borderBottomWidth:2,
        borderBottomColor:'#ec223f'
    }

});

 const mapStateToProps=(state)=>{
     return{
         isPlaying:state.player.isPlaying,
         currentPlayingAlbum:state.player.currentPlayingAlbum
     }
 }
  export default connect(mapStateToProps)(FooterComponent);
