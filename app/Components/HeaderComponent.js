import React, { Component } from 'react';
import {
    View, Text, StyleSheet, Image, ImageBackground, TouchableWithoutFeedback, TouchableOpacity,
    Dimensions,KeyboardAvoidingView
} from 'react-native';
import {TextInput} from 'react-native';
import {Header,SearchBar,Icon} from 'react-native-elements';
import {userActions} from "../store/actions/dashboard";
import SearchComponent from './SearchComponent'
import {connect} from 'react-redux';
import {NavigationActions} from 'react-navigation';
const { width } = Dimensions.get('window');

 class HeaderComponent extends Component {
    constructor(props){
        super(props);
        this.state={
            search:false
        }

    }

    search=(text)=>{
        debugger
        if(text.length>=3) {
            this.props.dispatch(userActions.searchdata(text));
            }
        this.setState({searchText: text});

    }


    onSearch=()=>{
        this.props.dispatch(userActions.setSearch());
        this.setState({search:true});
        const search= NavigationActions.navigate({
            routeName: "Search"
        });
        this.props.navigation.dispatch(search);

    }
    drawer=()=>{

        this.props.navigation.navigate('DrawerToggle');

}
 componentDidUpdate(){

 }
 componentWillUnmount(){
        debugger
 }
componentWillReceiveProps(nextProps){
        debugger
        if(!nextProps.text){
            this.setState({searchText:''});

        }
        if(nextProps.searchbar===false){
            this.setState({search:false});
    }

}
     render() {


         return (

             <View style={{height:'10%'}}>
                 {this.state.search?

                     <Header backgroundColor='#242424' outerContainerStyles={Styles.header}
                             innerContainerStyles={Styles.inner}>
                         <View>
                             <Icon color={'#fff'} underlayColor={'#242424'} name={'arrow-back'} onPress={()=>{this.setState({search:false}) ,this.props.navigation.dispatch(NavigationActions.back())}} />
                         </View>

                         <View style={Styles.Search}>
                             <SearchBar
                                 noicon={true}
                                 clearicon
                                 containerStyle={{width:250}}
                                 placeholder='Search Album, Artists, Songs...' value={this.state.searchText}  onChangeText={(text)=>this.search(text)} />

                         </View>
                     </Header>
                  :
                 <Header backgroundColor='#242424' outerContainerStyles={Styles.header}
                         innerContainerStyles={Styles.inner}>
                <View>
                 <Icon name={'menu'} color={'#fff'} underlayColor={'#242424'}  onPress={this.drawer}/>
                </View>
                 <View style={Styles.Brand} >
                     <Text style={{color:'#ec223f',fontFamily:'ubuntu-R',fontSize:16,fontWeight:'bold'}}>VOISE</Text>
                       {/*<Image style={{width:40,height:40}} source={require('../login/voise_logo.png')} />*/}

                 </View>
                     <TouchableOpacity onPress={this.onSearch}>
                         <View pointerEvents='none' style={{marginLeft:'70%',alignItems:'flex-end'}}>
                             <Icon name={'search'} color={'#fff'} underlayColor={'#242424'} iconStyle={{marginLeft:'1%',alignSelf:'flex-end'}} />
                         </View>
                     </TouchableOpacity>

                 </Header>
                 }
             </View>
         );
     }
 }
 const Styles=StyleSheet.create({
    header:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-start',
        borderBottomWidth:0
        // justifyContent:'flex-start'
        },
     inner:{
         justifyContent:'flex-start',
         marginHorizontal:-5,
         alignItems:'center'
     },
     Search:{
          borderBottomWidth: 1,
         // paddingBottom: 1,
         marginLeft:'15%',
         marginVertical:8,
         paddingVertical:4,
         width:250,
         position:'absolute',
         // borderRadius:110,
         // borderWidth:0.5,
          borderColor: '#000',
         flexDirection:'row',





     },
     Text:{
         fontFamily:'Ubuntu-R',
         fontSize:15,
         color:'white'
     },
     Brand:{
        marginLeft:10

     }
});
const mapStateToProps=(state)=>{
    return{
        search:state.dashboard.search,
        text:state.dashboard.text,
        searchbar:state.dashboard.searchbar
    }
}

export default connect(mapStateToProps)(HeaderComponent);
