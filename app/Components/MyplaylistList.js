


import React,{Component} from 'react';
import {View, Text, ImageBackground, ScrollView, Dimensions,TouchableWithoutFeedback,TextInput,ToastAndroid,ActivityIndicator} from 'react-native';
import {Button} from 'react-native-elements';
import {Icon} from 'react-native-elements';
const { width } = Dimensions.get('window');
import {connect} from 'react-redux';
import {userActions} from "../store/actions/dashboard";
import ListPopover from 'react-native-list-popover';
const items=['Delete-Playlist','cancel'];
export default class MyplaylistList extends Component {
    constructor(props){
        super(props);
        this.state={
            show:false
        }

    }

    render() {
        return (
            <View key={this.props.i_playlist}>
                <TouchableWithoutFeedback
                onPress={() => this.setState({show: true})}>
                    <View style={{
                        flex: 1,
                        borderWidth: 0.2,
                        backgroundColor: '#000',
                        width: width - 16,
                        marginLeft: 8,
                        height: 60,
                        justifyContent: 'space-between',
                        marginTop: 8,
                        flexDirection: 'row'
                    }} key={this.props.i_playlist}>
                        <Text style={{
                            justifyContent: 'flex-start',
                            alignSelf: 'center',
                            color:this.state.show?'#f47b21' :'white',
                            fontSize: 16,
                            marginLeft: 8
                        }}>
                            {this.props.playlist_name}
                        </Text>
                        {this.state.show?<Icon size={18} iconStyle={{
                            alignSelf: 'center',
                            justifyContent: 'flex-end',
                            marginLeft: 8,
                            marginRight: 8,
                            marginTop: 1
                        }} type='feather' name='chevrons-down'  onPress={() => this.setState({show:false})} color='#f47b21'/>:<Icon size={18} iconStyle={{
                            alignSelf: 'center',
                            justifyContent: 'flex-end',
                            color: 'white',
                            marginLeft: 8,
                            marginRight: 8,
                            marginTop: 1
                        }} type='feather' name='chevrons-right' color='white'/> }

                    </View>

                </TouchableWithoutFeedback>

                <View key={this.props.i_playlist}>
                    {this.state.show &&
                    <View key={this.props.i_playlist} style={{backgroundColor: '#2f3032', width: '100%', height: 30}}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'space-around',
                            marginHorizontal: 10
                        }}>
                            <Icon name='chevron-with-circle-right' type='entypo' color='#f47b21' onPress={() => {
                                this.props.playListAction(this.props.i_playlist)
                            }}/>
                            <Icon name='circle-with-cross' type='entypo' color='#f47b21'  onPress={() => this.setState({show:false})}/>
                            <Icon name='trash' type='entypo' color='#f47b21'  onPress={() => {
                                this.props.delete(this.props.i_playlist)}}/>

                        </View>
                    </View>
                    }
                </View>
            </View>
        )
    }
}