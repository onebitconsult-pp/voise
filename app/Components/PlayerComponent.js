import React, {
    Component
} from 'react';

import {
    Alert,
    Image,
    AppRegistry,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Modal,
    AsyncStorage,
	TouchableHighlight,
    Button
} from 'react-native';
import {playerAction} from "../store/actions/playerAction";
import Controls from './controls';
import MusicControl from 'react-native-music-control';
import {connect} from "react-redux";
import Sound from 'react-native-sound';
import _ from 'lodash';

class PlayerComponent extends Component {
    constructor(props){
        super(props);
        this.sounds={};
        this.state={
            duration:0,
            current:0,
	        showControls: false,
            repeat:false,
            showModal: false,
            shuffle:false,
            Completed:false
        }
    this.pauseSong=this.pauseSong.bind(this);
    this.playSong=this.playSong.bind(this);

    }
    componentWillMount(){
        debugger
    }
    playnextsong(id){
        if(this.props.currentPlayingAlbum){
            if (!id) {
                id = this.props.songDetails.id;
            }

            if (this.state.shuffle) {
                let allSongsLength = this.props.currentPlayingAlbum.song_list.length;
                id = Math.floor((Math.random() * allSongsLength) + 1);

            }
            if (id < this.props.currentPlayingAlbum.song_list.length - 1) {

                id += 1;
                params = this.props.currentPlayingAlbum.song_list[id];
                params.url=this.props.currentPlayingAlbum.song_list[id].song_link;
                params.id = id;
            } else {

                id = 0;
                params = this.props.currentPlayingAlbum.song_list[0];
                params.url=this.props.currentPlayingAlbum.song_list[0].song_link;
                params.id = id;
            }


            this.playSong(params);
        }

    }
	toggleRepeat = () => {
        this.setState({repeat: !this.state.repeat}, () => {
            if (this.state.repeat) {
	            global.sounds.setNumberOfLoops(-1);
            } else {
	            global.sounds.setNumberOfLoops(0);
            }
        });
    }
	toggleshuffle = () => {
        this.setState({shuffle: !this.state.shuffle});
    }
    playprevsong(id){
        if(this.props.currentPlayingAlbum) {
            if (id < this.props.currentPlayingAlbum.song_list.length - 1 && id > 0) {
                id -= 1;
                params = this.props.currentPlayingAlbum.song_list[id];
                params.url=this.props.currentPlayingAlbum.song_list[id].song_link;
            } else {
                params = this.props.currentPlayingAlbum.song_list[0];
                params.url=this.props.currentPlayingAlbum.song_list[0].song_link;
            }
            this.playSong(params);
        }

    }
    pauseSong=(params)=>{

        if(arguments.length==0){
            this.setState({current:0});
            this.props.dispatch(playerAction.pauseSong());
            global.sounds.pause();
        }else{
            this.props.dispatch(playerAction.pauseSong());
            if(!_.isEmpty((global.sounds))){

            global.sounds.pause();
            }
            return new Promise((resolve,reject)=>{

                resolve(params);
            })

        }

        // MusicControl.stopControl()


    }
    playSong=(params)=> {

        if (params) {
            if (this.props.isPlaying === true) {

                this.pauseSong(params).then((params) => {
                    return this.playSon(params)
                });
            } else {

                this.playSon(params);
            }

        } else {
            Alert.alert('Select', 'Please select The album first', [
                {text: 'select album'}

            ])


        }
    }
    continueSongPlay=()=>{
        if(!this.state.Completed) { this.tickInterval = setInterval(() => { this.tick(); },500); }
            global.sounds.play((success) => {
	            if(success){
	                this.pauseSong();
	                global.sounds.release();
	                this.setState({Completed:true},()=>{this.setState({current:0},()=>{this.setState({imageUrl:''})})});
	                if(this.props.songDetails.purchased == 0) {
		                this.toggleModal(!this.state.showModal);
	                } else {
		                this.playnextsong();
	                }
                }

            });



    }
    tick() {

            if (!_.isEmpty((global.sounds))) {
                global.sounds.getCurrentTime((seconds) => {
                    if (this.tickInterval) {
                        this.setState({
                            current: seconds,
                        });
                    }
                });

        }
    }
    seek=(seekduration)=>{
        global.sounds.setCurrentTime(seekduration);
        this.setState({
            current:seekduration,
        });
    }



    playSon=(params)=>{

        this.setState({Completed:false});
if(_.isEmpty((global.sounds))){

    this.setState({imageUrl:params.imageUrl});
    this.setState({songName:params.songName},()=>console.log(this.state.songName));
    global.sounds=new Sound(params.url,undefined,error => callback(error));
    const callback = (error) => {

        if (error) {

            return;
        }
        //this.sounds.stop();
        if(this.props.isPlaying===false) {

            this.props.dispatch(playerAction.playSong(params));
        }

        this.continueSongPlay();
         this.setState({duration:global.sounds.getDuration()});


    };

} else {
    this.props.dispatch(playerAction.playSong(params));

    this.continueSongPlay();
}





    };

    componentWillReceiveProps(nextProps)
    {
        debugger
        // if(!_.isEmpty(nextProps)){
        //     this.setState({showControls: true});
        // }
        if(!_.isEqual(nextProps.songDetails,this.props.songDetails)){
            if (this.props.isPlaying === true){

                this.pauseSong(this.props.songDetails).then((params) => {
                    delete global.sounds;
                });
            }else{
                delete global.sounds;
            }

            this.playSong(nextProps.songDetails);
        }
    }
    toggleModal = (modalStatus) => {
        this.setState({showModal: modalStatus});
    }
    purchaseSong = () => {

	    this.toggleModal(!this.state.showModal);

	    this.props.showWallet();
    }
	cancelPurchase = () => {
		this.toggleModal(!this.state.showModal);
		this.playnextsong();
	}
    render(){
        return(
            <View style={{flex:1,backgroundColor:'#000'}}>
	            {/*{this.state.showControls ?*/}
                    <View style={{marginBottom:16}}>
                        <View style={{height:16,alignItems:'center',marginBottom:8}}>
                            <Text style={{fontSize:14,color:'#fff'}}>{this.state.songName}</Text>
                        </View>

                        <View style={{flex:1,flexDirection:'row'}}>

                        <View style={{backgroundColor:'#000',width:'10%',height:60,alignItems:'center',justifyContent:'center'}}>

                          <Image style={{marginLeft:10,borderRadius:60,height:32,width:30}} source={{uri:this.state.imageUrl}} />

                        </View>
                    <View style={{flex:1,justifyContent:'center',height:60,backgroundColor:'rgba(0,0,0,0)'}}>
		            <Controls onSeek={this.seek} duration={this.state.duration ? this.state.duration : 0}
		                      current={this.state.current} playSongs={() => this.playSong(this.props.songDetails)}
                              Completed={this.Completed}
		                      playNext={() => this.playnextsong(this.props.songDetails.id)}
		                      playPrev={() => this.playprevsong(this.props.songDetails.id)}
		                      pauseSongs={this.pauseSong}
		                      toggleRepeat = {this.toggleRepeat}
                              repeat={this.state.repeat}
                              toggleShuffle = {this.toggleshuffle}
                              isShuffle = {this.state.shuffle}
                    />
                    </View>
                        </View>
                    </View>
		            {/*:*/}
		            {/*<View style={{height:1}}>*/}
                        {/*<Text style={{color: 'white', justifyContent:'center', textAlign: 'center'}}>Please select an Album</Text>*/}
                    {/*</View>*/}
	            {/*}*/}
	            {/*<Modal*/}
		            {/*animationType="slide"*/}
		            {/*transparent={true}*/}
		            {/*visible={this.state.showModal}*/}
		            {/*onRequestClose={() => {*/}
			            {/*alert('Modal has been closed.');*/}
		            {/*}}>*/}
		            {/*<View style={{flex:1,alignItems:'center',justifyContent:'center'}}>*/}
			            {/*<View style={{backgroundColor: 'rgba(255, 255, 255, 0.5)', paddingTop: 16, paddingBottom: 16}}>*/}
				            {/*<Text style={{textAlign:'center'}}>Do you want to purchase song ?</Text>*/}
				            {/*<View style={{flexDirection: 'row',justifyContent:'center',marginTop:16}}>*/}
					            {/*<View*/}
						            {/*style={{marginHorizontal:16}}*/}
					            {/*>*/}
					            {/*<Button*/}
						            {/*onPress={this.purchaseSong}*/}
						            {/*title="Purchase"*/}
						            {/*color="blue"*/}
						            {/*accessibilityLabel="Purchase song"*/}

					            {/*/>*/}
					            {/*</View>*/}
					            {/*<View*/}
						            {/*style={{marginHorizontal: 16}}*/}
					            {/*>*/}

					            {/*<Button*/}
						            {/*onPress={this.cancelPurchase}*/}
						            {/*title="Cancel"*/}
						            {/*color="#ccc"*/}
						            {/*accessibilityLabel="do not purchase song"*/}
					            {/*/>*/}
					            {/*</View>*/}
                            {/*</View>*/}



			            {/*</View>*/}
		            {/*</View>*/}
	            {/*</Modal>*/}
            </View>
        );
    }


}
const mapStateToProps= (state) => {
    return {
        isPlaying: state.player.isPlaying,
        songDetails:state.player.songDetails,
        currentPlayingAlbum:state.player.currentPlayingAlbum,
    };
}

export default connect(mapStateToProps)(PlayerComponent);
