import React,{Component} from 'react';
import {View, Text, Image, ScrollView, StyleSheet, Dimensions,ActivityIndicator, ToastAndroid,TouchableWithoutFeedback} from 'react-native';
import { List, ListItem } from 'react-native-elements'
import {connect} from 'react-redux';
import {userActions} from "../store/actions/dashboard";
import {playerAction} from "../store/actions/playerAction";
import ListPopover from 'react-native-list-popover';
const items=['remove-song','cancel'];

 class PlaylistSongs extends Component {
    constructor(props){
        super(props);
        this.state={
            playListSongs:[],
            visible:false,
            list: [


            ],
            toast:false,
            loading:true
        }
    }
     play=(id,url)=>{
         let params={id:id,url:url};
         this.props.dispatch(playerAction.playSong(params));

     }
    componentWillMount(){

        this.setState({loading:true});
        if(this.props.navigation.state.params!==undefined) {
            this.setState({playlist:this.props.navigation.state.params.id});
            this.props.dispatch(userActions.playListSongs(this.props.navigation.state.params.id));
        }
    }
     componentDidMount(){

         this.setState({loading:true});
     }
     componentDidFocus(){

     }
    onLongPress=(playlistid,songid)=>{
        this.setState({currentId:playlistid,currentSongId:songid,visible:true});
    }
     clickPopover=(item)=>{
         this.setState({item:item},()=>{
             if(this.state.item=='remove-song'){
                 this.props.dispatch(userActions.deletesongPlaylist(this.state.currentId,this.state.currentSongId));
             } else{
                 this.setState({visible:false})
             }
         });

     }
    componentWillReceiveProps(nextProps){

        if(nextProps.playListSongs==undefined){
            this.setState({loading:false});
            this.setState({list:[]});
        }else if(nextProps.playListSongs.songs_list!==undefined){
            this.setState({list:nextProps.playListSongs.songs_list});
        }

         if(nextProps.playListSongs!==undefined){
             this.setState({loading:false});
            this.setState({playlistName:nextProps.playListSongs.playlist_info[0].playlist_name});
        }


        if(nextProps.message!==undefined && this.state.toast===false){
                this.setState({toast:true});
            this.props.dispatch(userActions.playListSongs(this.state.playlist));
            ToastAndroid.show('deleted successfully',ToastAndroid.SHORT);

        }
    }



    render(){
        return(

            <View style={{flex:1,backgroundColor:'black'}}>
                { this.state.loading? <View style={{flex:1,justifyContent:'center'}}>
                        <ActivityIndicator size="large"/>


                    </View>:
                    <View>
                <View style={{justifyContent:'center',alignItems:'center',marginTop:14,flexDirection:'row'}}>
                    { this.state.playlistName && <Text style={{color:'white',fontSize:16,fontFamily:'Ubuntu-R'}}>
                        {this.state.playlistName}
                    </Text> }
                </View>
                  <View style={{justifyContent:'center',alignItems:'center',marginTop:8}}>
                    <TouchableWithoutFeedback onPress={()=>{this.props.navigation.navigate('Home')}}>
                    <View style={{alignSelf:'center'}}>
                    <Text style={{color:'#ec223f',alignSelf:'center'}}>
                      Add Songs To Playlist
                    </Text>
                    </View>
                    </TouchableWithoutFeedback>
                    {/*<Image style={Styles.imgcenter} source={{uri:'https://images.pexels.com/photos/248797/pexels-photo-248797.jpeg?h=350&auto=compress&cs=tinysrgb'}}/>*/}
                </View>
                <View>
                    <ScrollView>
                        <ListPopover
                            list={items}
                            isVisible={this.state.visible}
                            onClick={(item) => {
                                this.setState({item: item}), this.clickPopover(item) } }
                            onClose={() => this.setState({visible: false})}/>
                        <List containerStyle={{marginBottom:10,backgroundColor:'transparent'}}>

                            {this.state.list.map((l, i) => (
                                    <ListItem
                                        titleStyle={{color:'#fff'}}
                                        subtitleStyle={{color:'#fff'}}
                                        roundAvatar
                                        avatar={{uri:l.album_cover}}
                                        key={i}
                                        title={l.song_name}
                                        subtitle={l.album_name}
                                        hideChevron={true}
                                        url={l.song_link}
                                        albumId={l.i_album}
                                        songId={l.i_song}
                                        onPress={()=>this.play(l.i_song,l.song_link)}
                                        onLongPress={()=>{this.onLongPress(this.props.navigation.state.params.id,l.i_song)}}
                                    />
                                ))
                            }
                        </List>
                    </ScrollView>
                </View>
                    </View>
                    }
            </View>

        );
    }
}

const Styles=StyleSheet.create({
    imgcenter: {
        width: 120,
        height: 120,
        borderRadius: 60


    },
});

const mapStateToProps=(state)=>{
    return {
        playListSongs:state.dashboard.listsongs,
        message:state.dashboard.message
    }
}
export default connect(mapStateToProps)(PlaylistSongs);