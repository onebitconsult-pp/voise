import React,{Component} from 'react';
import {View,Text} from 'react-native';
import ListPopover from 'react-native-list-popover';

export default class PPopover extends Component {
    constructor(props){
        super(props);
        this.state={
            visible:false,
            buttonRect: {},
        }


    }
    showPopover() {
        this.refs.button.measure((ox, oy, width, height, px, py) => {
            this.setState({
                visible: true,
                buttonRect: {x: px, y: py, width: width, height: height}
            });
        });
    }

    closePopover() {
        this.setState({visible: false});
    }

    render(){
        return (
            <View>
        <ListPopover
            list={['addtoPlayList','SongDetails']}
            isVisible={this.state.visible}
                     onClose={this.closePopover}>

        </ListPopover>
            </View>
        );
    }
}