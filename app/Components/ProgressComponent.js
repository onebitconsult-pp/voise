import React , {Component} from 'react';
import {View,Text,Slider,Dimensions} from 'react-native';
import {connect} from 'react-redux';
import {playerAction} from '../store/actions/playerAction';

 class ProgressComponent extends Component {
    constructor(props){
        super(props);
        this.state={
            current:0,
            mounted:false,
            curdur:'-:--',
            dur:'-:--'
        }



    }
    seek=(seeekduration)=>{
        debugger
        this.props.onSeek(seeekduration);

    }
     Complete=()=>{
        debugger

     }





    componentDidMount(){
        console.log(this.props.current);
        this.setState({mounted:true});





    }
    componentDidUpdate(){

    }


    componentWillReceiveProps(nextProps){



        if(nextProps.isPlaying===true) {

            var minutes = Math.floor( this.props.duration/ 60);

            var seconds = this.props.duration - (minutes * 60);
            seconds = Math.round((seconds * 100) / 100).toFixed(2);
            seconds=Math.trunc(seconds);
              let durtime=minutes+':'+seconds;
            this.setState({dur:durtime});
           // {this.props.currentTime && this.setState({current:nextProps.currentTime})}

            var curminutes = Math.floor( this.props.current/ 60);

            var curseconds = this.props.current - (curminutes * 60);
            curseconds = Math.round((curseconds * 100) / 100).toFixed(3);
            curseconds=Math.trunc(curseconds);
            if(curseconds<10){
                curseconds='0'+curseconds
            }else{
                curseconds=curseconds
            }
            let curtime=curminutes+':'+curseconds;
            this.setState({curdur:curtime});


        }
        }

        componentDidUpdate(prevprops,previousstate){



        }

     componentWillUnmount(){


     }






    render() {

        const window = Dimensions.get('window');
        return (
            <View style={{flex:1,flexDirection:'row',marginHorizontal:8}}>
                <View>
                    <Text style={{color:'#ec223f',fontSize:14}}>{this.state.curdur}</Text>
                </View>
                  <View style={{flex:1}}>
                    <Slider
                        step={0.05}
                        style={{height:15}}
                        minimumValue={1}
                        maximumValue={this.props.duration}
                        minimumTrackTintColor={'#ec223f'}
                        maximumTrackTintColor={'#fff'}
                        thumbTintColor={'#fff'}
                        value={this.props.current}
                        onValueChange={this.seek}
                        onSlidingComplete={this.Complete}

                    />
                  </View>
                <View>
                    <Text style={{color:'#696969',fontSize:14}}>{this.state.dur}</Text>
                </View>


            </View>
        );


    }
}
const mapStateToProps=(state)=>{
   return{ isPlaying: state.player.isPlaying,
        currentTime:state.player.time}
}
export default  connect(mapStateToProps)(ProgressComponent);