import React,{Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Header,SearchBar,Icon} from 'react-native-elements';
import {connect} from 'react-redux';
import {userActions} from "../store/actions/dashboard";
import {NavigationActions} from 'react-navigation';


class SearchComponent extends Component {

    back=()=>{
        this.props.dispatch(userActions.cancelSearch());
        // this.props.navigation.navigate('Home');
    }

    render(){
        return(
            <View style={{height:'14%'}}>

                    <Header backgroundColor='#242424' outerContainerStyles={Styles.header}
                            innerContainerStyles={Styles.inner}>
                        <View>
                            <Icon color={'#fff'} underlayColor={'#242424'} name={'arrow-back'} onPress={this.back()} />
                        </View>

                        <View style={Styles.Search}>
                            <SearchBar
                                containerStyle={{width:240}}
                                inputStyle={Styles.Search}
                                placeholder='Albums, Artists, Songs' />

                        </View>
                                    </Header>

            </View>

        )
    }


}
const Styles=StyleSheet.create({
    header:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-start'
    },
    inner:{
        justifyContent:'flex-start',
        marginBottom:0,
        alignItems:'center'
    },
    Search:{
        marginLeft:'6%',
        width:260,
        borderColor: '#fff',
        flexDirection:'row',
    },
    Text:{
        fontFamily:'Ubuntu-R',
        fontSize:15,
        color:'white'
    },
    Brand:{

        marginLeft:'4%',

    }
});
const mapStateToProps=(state)=>{
    return {
        search:state.dashboard.search
    }
}


export default connect(mapStateToProps)(SearchComponent);
