
import React, { Component } from 'react';
import {Image,TouchableOpacity,TouchableWithoutFeedback,Text} from 'react-native';
import { connect } from 'react-redux';
import { View, } from '@shoutem/ui';
import {Icon} from 'react-native-elements';
import {playerActions} from '../store/actions/playerAction';
import ProgressComponent from './ProgressComponent';

// import {
//    playerAction
// } from './actions';

 class Controls extends Component {
    constructor(props){
        super(props);
        this.playSong=this.playSong.bind(this);
    }
    playSong(){

     this.props.playSongs();

    }
    pauseSong(){
        this.props.pauseSongs();

    }
    playNext(){
        this.props.playNext();

    }
    playPrevious(){
        this.props.playPrev();
    }
    seek=(duration)=>{

        this.props.onSeek(duration);
    }
     Completed=()=>{
        this.props.Completed();
    }
    render(){
        return(
            <View style={{flex:1}}>
                <View style={{flex:1,height:20}}>

                <ProgressComponent onSeek={this.seek} duration={this.props.duration} current={this.props.current}  Completed={this.Completed}/>

                </View>
        <View styleName="horizontal space-between" style={{height:45,backgroundColor:'transparent',alignItems:'center',justifyContent:'center'}}>
            <Icon name="shuffle" onPress = {this.props.toggleShuffle} underlayColor={'#242424'}  color={this.props.isShuffle?'#ec223f':'#fff'} iconStyle={{marginHorizontal:18}} size={26}  />
            <Icon name="navigate-before" underlayColor={'#242424'} iconStyle={{marginHorizontal:18}} color={'#fff'} size={36} onPress={() => this.playPrevious()} />


            {this.props.isPlaying ?

                <TouchableWithoutFeedback onPress={() => this.pauseSong()}>
                <View>
                <Image source={require('../images/voisepause.png')} style={{width:32,height:35}}/>
                </View>
                  </TouchableWithoutFeedback>
                   :
                 <TouchableWithoutFeedback onPress={() => this.playSong()}>
                <View>
                  <Image source={require('../images/voiseplay.png')} style={{width:32,height:35}} />
                </View>
                 </TouchableWithoutFeedback>

            }



            <Icon name="navigate-next" color={'#fff'} underlayColor={'#242424'} iconStyle={{marginHorizontal:18}} size={36} onPress={() => this.playNext()} />
              <Icon name="repeat" color={this.props.repeat?'#ec223f':'#fff'} underlayColor={'#242424'} iconStyle={{marginHorizontal:18}} size={26} onPress={() => this.props.toggleRepeat()} />

        </View>
            </View>
        );

    }

}

const mapStateToProps= (state) => {
    return {
        isPlaying: state.player.isPlaying
    };
    }

export default connect(mapStateToProps)(Controls);
