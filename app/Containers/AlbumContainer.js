import React ,{Component} from 'react';
import {ToastAndroid, View,Text,ImageBackground,Image,StyleSheet,ScrollView,TouchableWithoutFeedback,Modal} from 'react-native';
import CardComponent from '../Components/CardComponent';
import {Icon} from 'react-native-elements';
import {connect} from 'react-redux';
import {userActions} from "../store/actions/dashboard";
import {playerAction} from "../store/actions/playerAction";
import PlayerComponent from '../Components/PlayerComponent';
import FooterComponent from '../Components/FooterComponent';
import ListPopover from 'react-native-list-popover';
const items=['addtoPlayList','cancel'];

 class AlbumContainer extends Component {
    constructor(props){
        super(props);
        this.state = {
            song_link:null,
            item:false,
            visible:false,
            names: [
                {'name': 'Eminem', 'id': 1,'song_link':'http://m4a-64.cdn107.com/44/03/98/4403985957274169186.m4a'},
                {'name': 'Rap god', 'id': 2},
                {'name': 'Monster', 'id': 3},
                {'name': 'Love the way you lie', 'id': 4},
                {'name': 'Not Afraid', 'id': 5},
                {'name': 'Lose yourself ', 'id': 6},
                {'name': 'guts over fear', 'id': 7},
                {'name': 'stan', 'id': 8},
                {'name': 'in ur head', 'id': 9},
                {'name': 'framed', 'id': 10},
                {'name': 'Castle', 'id': 11},
                {'name': 'Need me', 'id': 12}
            ]
        }

    }
    callapi=(params)=>{


        this.props.dispatch(playerAction.playSong(params));
        this.props.dispatch(playerAction.currentAlbum(this.state.details));
    }
    componentWillMount(){
        debugger
        this.props.dispatch(userActions.albums(this.props.navigation.state.params.id));


    }
    componentWillReceiveProps(nextprops){

       debugger
        if(nextprops.albumdetails!==undefined) {
            this.setState({details: nextprops.albumdetails.res});
        }


        if(nextprops.id!==undefined){
            this.props.navigation.navigate('MyPlaylist',{'songid':this.state.songId});
        }
    }
    componentDidMount(){
        debugger
    }
    componentDidUpdate(){

        if(!!this.state.details.song_list===true && !this.state.song){

            this.setState({song:this.state.details.song_list,empty:false});
        }
    }
     Popover=(songId,url,song,artist)=>{

         this.setState({visible:!this.state.visible,songId:songId,modalUrl:url,modalSong:song,modalArtist:artist});
     }
     clickPopover=(id)=>{

        this.props.dispatch(userActions.selctedSong(this.state.songId));
         this.setState({visible: false});



     }
    render(){
        return(

            <View style={{flex:1}}>
                <ImageBackground
                    style={{
                        backgroundColor: '#000',
                        flex: 1,
                        width: '100%',
                        position:'absolute',
                        height: '100%',
                        justifyContent: 'center',

                    }}
                    source={require('../images/albumback.png')}>
                    <View style={{flex:1}}>
                        <Modal
                            animationType="slide"
                            transparent={true}
                            visible={this.state.visible}
                            onRequestClose={() => {
                                this.setState({visible: false});
                            }}>
                            <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'rgba(0,0,0,0.9)'}}>
                                <View style={{width:'100%',backgroundColor:'#2f3032',height:40}}>
                                    <TouchableWithoutFeedback onPress={() => this.setState({visible: false})}>
                                        <View style={{marginTop:8,marginHorizontal:10,flexDirection:'row',justifyContent:'flex-start',alignItems:'center'}}>
                                            <Icon type='feather' name='chevrons-left' color='#f47b21'/>
                                            <Text style={{color:'#f47b21',fontSize:14}}>    GO BACK</Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                                <View style={{height:150,backgroundColor:'transparent'}}>
                                    <Image source={{uri:this.state.modalUrl}} style={{height:150,width:150}}/>

                                </View>
                                <View style={{height:48,backgroundColor:'transparent',alignItems:'center',justifyContent:'center'}}>

                                    <Text style={{color:'#fff',fontSize:12,fontFamily:'ubuntu-R',marginTop:8}}>{this.state.modalSong}</Text>
                                    <Text style={{color:'#fff',fontSize:12,fontFamily:'ubuntu-R',marginTop:8}}>{this.state.modalArtist}</Text>

                                </View>

                                <View style={{width:'100%',backgroundColor:'#2f3032',height:40,justifyContent:'center'}}>
                                    <View style={{marginHorizontal:10,flexDirection:'row',justifyContent:'space-around',alignItems:'center'}}>
                                        <Icon type='feather' size={26} name='chevron-right' color='#f47b21' onPress={() => this.clickPopover(this.state.songId)}/>
                                        <Icon type='evilicon' size={26} name='close-o' color='#f47b21'  onPress={() => this.setState({visible: false})}/>


                                    </View>
                                </View>
                            </View>
                        </Modal>

                        <ScrollView style={{flex:1}}>


                    <View style={{flex:0.6,flexDirection:'row',marginTop:30}}>



                        <View style={Styles.imgview}>
                            {this.state.details &&  <ImageBackground style={Styles.imgcenter} source={require('../images/shadow.png')}>


                                <Image  style={Styles.imgcenter} source={{uri:this.state.details.song_list[0].album_cover}} >

                                </Image>





                            </ImageBackground> }
                            {this.state.details && <Text  style={{color:'#fff',alignSelf:'center',marginTop:1,fontFamily:'Ubuntu-R'}}>{ this.state.details.album_details.name} </Text>}

                        </View>




                    </View>
                    <View style={Styles.list}>
                        <Text style={{color:'#fff',alignSelf:'center'}}>Top Song List</Text>
                        {this.state.empty && <Text style={{color:'#fff',alignSelf:'center'}}>{this.state.empty}</Text>}
                        {this.state.details && <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={{marginTop:5,padding:3}}>


                            {this.state.song && this.state.song.map((item, index) => (
                                <View key={item.id}>
                                    <TouchableWithoutFeedback key={item.id}>
                                        <CardComponent icon={true} popover={this.Popover} ids={item.i_song} key={item.i_song} onPlay={this.callapi}  id={index}
                                                       Imgurl={item.album_cover} url={item.song_link}
                                                       imagestyle={Styles.image} style={Styles.center} Text={item.song_name} duration={item.str_duration}
                                                       navigation={this.props.navigation}/>
                                    </TouchableWithoutFeedback>
                                </View>
                            ))
                            }








                        </ScrollView> }
                    </View>


                        </ScrollView>
                </View>







                </ImageBackground>











            </View>
        );
    }

}
const Styles=StyleSheet.create({
    imgcenter:{
        width:120,
        height:120,
        borderRadius:60



    },
    imgview:{
        flex:1,
        overflow: 'hidden',
        borderRadius:60,
        alignItems:'center',


    },
    Text:{
        marginTop:20,
        padding:17,

    },
    list:{
        flex:0.7,
        backgroundColor:'transparent',


    },
    center: {
        // padding: 0, margin: 1,
        // height: 160,
        // width: 130,
        // marginRight: 4,
        // borderRadius: 4,
        // borderColor: '#000',
        // backgroundColor:'transparent',


    },
    image: {

        minHeight:120,
        width: 130,
        height: 120,
        marginTop:0,
        paddingTop:2

    },
    scrollimage:{
        maxHeight:100,
        maxWidth:100,
    },
    button:{
        backgroundColor:'#0295f7',
        alignSelf:'center',
        width:'50%',
        justifyContent:'center',
        height:40,
        overflow:'hidden',
        borderRadius:4,

    },
    btntext:{
        fontSize:20,
        color:'#ffffff',
        fontFamily:'Ubuntu-R',
        textAlign: 'center',
        alignItems:'center',
        justifyContent:'center'

    },
    buttonview:{
        flex:0.1,
        marginTop:20,
        marginBottom:60

    },
    header:{
        height:55,
        backgroundColor:'#242424',
        justifyContent:'center'
    },
    inner:{
        justifyContent:'center'
    }

});


const mapStateToProps=(state)=>{
    return {
        albumdetails:state.dashboard.albumdetails,
        id:state.dashboard.id
    }

}
export default connect(mapStateToProps) (AlbumContainer);
