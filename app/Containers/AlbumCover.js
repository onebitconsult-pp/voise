import React ,{Component} from 'react';
import {View,Text,ImageBackground,Image,StyleSheet,ScrollView,TouchableWithoutFeedback} from 'react-native';
import CardComponent from '../Components/CardComponent';
import {Header,Icon} from 'react-native-elements';
import HeaderComponent from '../Components/HeaderComponent';
import FooterComponent from '../Components/FooterComponent';
import AlbumContainer from './AlbumContainer';
import {userActions} from "../store/actions/dashboard";
import {Api} from '../api';


export default  class AlbumCover extends Component {
    constructor(props){

        super(props);
        debugger
        this.id=this.props.navigation.state.params.id;
        // this.state.id=this.props.navigation.state.params.id;
    }
    componentWillMount(){


    }
    render(){
        return(
            <View style={{flex:1}}>



                <AlbumContainer id={this.id}  navigation = {this.props.navigation}/>














            </View>

        );
    }






}
const Styles=StyleSheet.create({
    imgcenter:{
        width:120,
        height:120,



    },
    imgview:{
        flex:1,
        overflow: 'hidden',
        borderRadius: 50,
        alignItems:'center',


    },
    Text:{
        marginTop:20,
        padding:17,

    },
    list:{
        flex:0.7,


    },
    center:{
        marginTop:10,
        padding:0,
        height:120,
        width:100,
        marginRight:3
    },
    scrollimage:{
        maxHeight:100,
        maxWidth:100,
    },
    button:{
        backgroundColor:'#0295f7',
        alignSelf:'center',
        width:'50%',
        justifyContent:'center',
        height:40,
        overflow:'hidden',
        borderRadius:4,

    },
    btntext:{
        fontSize:20,
        color:'#ffffff',
        fontFamily:'Ubuntu-R',
        textAlign: 'center',
        alignItems:'center',
        justifyContent:'center'

    },
    buttonview:{
       flex:0.2
    },
    header:{
        height:55,
        backgroundColor:'#242424',
        justifyContent:'center'
    },
    inner:{
        justifyContent:'center'
    }

});