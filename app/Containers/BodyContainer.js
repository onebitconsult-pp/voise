import React, { Component } from 'react';
import {
    View, Text, StyleSheet, Image, ImageBackground, TouchableWithoutFeedback, ScrollView, FlatList, Slider,
    ToastAndroid, Dimensions,Modal,ActivityIndicator
} from 'react-native';
import ListPopover from 'react-native-list-popover';
import TopScreen from './Slider';
import CardComponent from '../Components/CardComponent';
import {connect} from 'react-redux';
import {userActions} from "../store/actions/dashboard";
import {playerAction} from "../store/actions/playerAction";
const items=['Addtoplaylist','cancel'];
import {Icon} from 'react-native-elements';



 class BodyContainer extends  Component {
    constructor(props){
        super(props);
        this.state={
            bodydetails:'',
            visible:false,
        }
        this.toAlbum=this.toAlbum.bind(this);
    }
    componentDidMount(){
        this.setState({loading:true});
        this.props.dispatch(userActions.getBody());

    }
     toplay=(params)=>{
        debugger
         this.props.dispatch(playerAction.playSong(params));
     }
    Popover=(songId,url,song,artist)=>{

            this.setState({visible:!this.state.visible,songId:songId,modalUrl:url,modalSong:song,modalArtist:artist});
    }

    toAlbum(params){
        debugger
        this.props.navigation.navigate('Album',{'id':params.id})
    }
    clickPopover=(id)=>{
        debugger

            this.props.dispatch(userActions.selctedSong(this.state.songId));
            this.setState({visible: false});


    }

     componentWillReceiveProps(nextProps){
        debugger
         this.setState({loading:false});
            this.setState({bodydetails:nextProps.bodydetails});
         if(nextProps.id!==undefined){
             this.props.navigation.navigate('MyPlaylist',{'songid':this.state.songId});
         }

    }

    render() {
        return (
            <View style={{flex:1,justifyContent:'center'}}>
                {

                this.state.loading ? <ActivityIndicator size="large"/> :

                <View style={{backgroundColor: '#000', flex: 1, marginBottom: 0}}>

                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <Modal
                            animationType="slide"
                            transparent={true}
                            visible={this.state.visible}
                            onRequestClose={() => {
                                this.setState({visible: false});
                            }}>
                            <View style={{
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center',
                                backgroundColor: 'rgba(0,0,0,0.9)'
                            }}>
                                <View style={{width: '100%', backgroundColor: '#2f3032', height: 40}}>
                                    <TouchableWithoutFeedback onPress={() => this.setState({visible: false})}>
                                        <View style={{
                                            marginTop: 8,
                                            marginHorizontal: 10,
                                            flexDirection: 'row',
                                            justifyContent: 'flex-start',
                                            alignItems: 'center'
                                        }}>
                                            <Icon type='feather' name='chevrons-left' color='#f47b21'/>
                                            <Text style={{color: '#f47b21', fontSize: 14}}> GO BACK</Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                                <View style={{height: 150, backgroundColor: 'transparent'}}>
                                    <Image source={{uri: this.state.modalUrl}} style={{height: 150, width: 150}}/>

                                </View>
                                <View style={{
                                    height: 48,
                                    backgroundColor: 'transparent',
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }}>

                                    <Text style={{
                                        color: '#fff',
                                        fontSize: 12,
                                        fontFamily: 'ubuntu-R',
                                        marginTop: 8
                                    }}>{this.state.modalSong}</Text>
                                    <Text style={{
                                        color: '#fff',
                                        fontSize: 12,
                                        fontFamily: 'ubuntu-R',
                                        marginTop: 8
                                    }}>{this.state.modalArtist}</Text>

                                </View>

                                <View style={{
                                    width: '100%',
                                    backgroundColor: '#2f3032',
                                    height: 40,
                                    justifyContent: 'center'
                                }}>
                                    <View style={{
                                        marginHorizontal: 10,
                                        flexDirection: 'row',
                                        justifyContent: 'space-around',
                                        alignItems: 'center'
                                    }}>
                                        <Icon type='feather' size={26} name='chevron-right' color='#f47b21'
                                              onPress={() => this.clickPopover(this.state.songId)}/>
                                        <Icon type='evilicon' size={26} name='close-o' color='#f47b21'
                                              onPress={() => this.setState({visible: false})}/>


                                    </View>
                                </View>
                            </View>
                        </Modal>
                    </View>

                    {/*<View style={{flexDirection:'row',flex:1,marginBottom:5,heig

                 <ListPopover
                            list={items}
                            isVisible={this.state.visible}
                            onClick={(item) => {
                                this.setState({item: item},()=>{
                                    this.clickPopover()
                                }) } }
                            onClose={() => this.setState({visible: false})}/>
                ht:140}}>*/}
                    {/*<TopScreen/>*/}

                    {/*</View>*/}


                    <View style={{width: '100%', marginTop: 16, marginBottom: 3}}>

                        <View style={{flex: 1, flexDirection: 'row', marginTop: 0}}>

                            <Text style={{fontFamily: 'Ubuntu-R', fontSize: 16, color: '#fff', marginLeft: 10}}>New
                                Albums</Text>
                            <Text style={{
                                fontFamily: 'Ubuntu-R',
                                fontSize: 16,
                                color: '#049ff2',
                                marginLeft: 'auto'
                            }}></Text>
                        </View>

                        <View>

                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}
                                        contentContainerStyle={{marginTop: 5, padding: 3}}>
                                {this.state.bodydetails.new && this.state.bodydetails.new.map((item, index) => (
                                    <View key={item.i_album}>

                                        <CardComponent key={item.i_album} icon={false} id={item.i_album}
                                                       popover={this.Popover} onCardPress={this.toAlbum}
                                                       style={Styles.center} Text={item.album_name}
                                                       Artist={item.artist_name} imagestyle={Styles.image}
                                                       Imgurl={item.album_cover} navigation={this.props.navigation}/>


                                    </View>
                                ))
                                }
                            </ScrollView>
                        </View>

                        <View style={{flex: 1, flexDirection: 'row', marginTop: 16}}>

                            <Text style={{fontFamily: 'Ubuntu-R', fontSize: 16, color: '#fff', marginLeft: 10}}>Trending
                                Songs</Text>
                        </View>
                        <View>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}
                                        contentContainerStyle={{marginTop: 5, padding: 3}}>
                                {this.state.bodydetails.trending && this.state.bodydetails.trending.map((item, index) => (

                                    <View key={item.song_link}>

                                        <CardComponent key={item.song_link}  url={item.song_link} icon={true}
                                                       ids={item.i_song} id={item.i_song} onPlay={this.toplay}
                                                       popover={this.Popover} style={Styles.center}
                                                       Text={item.album_name} Artist={item.artist_name}
                                                       imagestyle={Styles.image} Imgurl={item.album_cover}
                                                       navigation={this.props.navigation}/>


                                    </View>
                                ))
                                }
                            </ScrollView>
                        </View>
                    </View>


                </View>
    }
            </View>
        );
    }
}
                const Styles =StyleSheet.create({
                    center: {
                        padding:0,
                        height: 160,
                        width: 130,
                        marginHorizontal: 8,
                        marginBottom:8,
                        borderRadius: 4,
                        borderColor: '#000'
                    },
                    image: {

                        width:130,
                        height: 120,


                    }



            });



 const mapStateToProps=(state)=> {
     return {
         bodydetails: state.dashboard.bodydetails,
         id:state.dashboard.id
     }
 }

 export default connect(mapStateToProps)(BodyContainer);
