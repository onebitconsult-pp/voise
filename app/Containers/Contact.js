import React ,{Component} from 'react';
import {View, TextInput,Linking,Text, Image, ImageBackground, TouchableOpacity, StyleSheet,ScrollView,ToastAndroid} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
export default class Contact extends Component{

    render(){
        return(
            <View style={{flex:1,backgroundColor:'#000'}}>
                <View style={{alignItems:'center'}}>
                    <Text style={{color:'#fff',fontFamily:'ubuntu-R',fontSize:24,marginLeft:8,marginBottom:16,marginTop:16}}>Contact Us</Text>
                </View>
               <View style={{flexDirection:'row',justifyContent:'flex-start',marginTop:8,marginLeft:8}}>

                       <Icon name='phone' color='white' size={26}/>
                       <Text style={{color:'#fff',fontFamily:'ubuntu-R',fontSize:16,marginLeft:10}}>+1 (613) 266-6476 </Text>




               </View>
                <View style={{flexDirection:'row',justifyContent:'flex-start',marginTop:8,marginLeft:8}}>

                    <Icon name='send' color='white' size={24}/>

                    <Text style={{color:'#fff',fontFamily:'ubuntu-R',fontSize:16,marginLeft:10}}  onPress={() => Linking.openURL('https://t.me/voisecom')}>https://t.me/voisecom</Text>




                </View>
                <View style={{flexDirection:'row',justifyContent:'flex-start',marginTop:8,marginLeft:8}}>
                        <Icon name='skype' color='white' size={26}/>
                        <Text style={{color:'#fff',fontFamily:'ubuntu-R',fontSize:16,marginLeft:10}} >voise.business</Text>

                </View>
                <View style={{flexDirection:'row',justifyContent:'flex-start',marginTop:8,marginLeft:8}}>

                        <Icon name='envelope' color='white' size={26}/>
                        <Text style={{color:'#fff',fontFamily:'ubuntu-R',fontSize:16,marginLeft:10}} onPress={() => Linking.openURL('mailto:business@voise.com')}>business@voise.com</Text>




                </View>


            </View>
        )
    }



}