import React,{Component} from 'react';
import {View, Text, Modal,ImageBackground, ScrollView, Dimensions,TouchableWithoutFeedback,TextInput,ToastAndroid,ActivityIndicator} from 'react-native';
import {Button} from 'react-native-elements';
import {Icon} from 'react-native-elements';
import MyplaylistList from '../Components/MyplaylistList';
const { width } = Dimensions.get('window');
import {connect} from 'react-redux';
import {userActions} from "../store/actions/dashboard";
import ListPopover from 'react-native-list-popover';
const items=['Delete-Playlist','cancel'];
class Myplaylist extends Component {
    constructor(props){
        super(props);
        this.state={
            userPlayLists:[],
            add:false,
            visible:false,
            toast:false,
            navigate:false,
            loading:true,
            show:false
        }

         }
    playListAction=(id)=>{
        debugger
        if(this.state.song_id!==undefined){
            this.props.dispatch(userActions.addToPlayList(id,this.state.song_id));
            this.setState({playlistid:id});
            this.setState({song_id:undefined});
          } else {
            this.props.navigation.navigate('PlaylistSongs', {'id': id});
        }

          }

    componentWillMount(){

          this.props.dispatch(userActions.getPlayLists());
        this.props.dispatch(userActions.selctedSong(undefined));

    }
    componentWillUnmount(){
        debugger
        this.props.navigation.state.routeName='MyPlaylist';
    }
    onAddPlaylist=()=>{
        debugger
        this.setState({add:!this.state.add});
}
    onlongPress=(id)=>{
        this.setState({currentId:id,visible:true});
    }
         addnew=()=>{
          this.props.dispatch(userActions.addPlayList(this.state.newlist));
         }
    clickPopover=(item)=>{
        this.setState({item:item},()=>{
                if(this.state.item=='Delete-Playlist'){
                    this.props.dispatch(userActions.deletePlaylist(this.state.currentId));
                } else{
                    this.setState({visible:false})
                }
        });

    }

    componentWillReceiveProps(nextProps){
        debugger
             if(nextProps.created===true){
                 this.props.dispatch(userActions.getPlayLists());
                 this.setState({add:false,newlist:''});
             }
             if(nextProps.userPlayLists!==undefined) {
                 this.setState({loading:false});
                 this.setState({userPlayLists: nextProps.userPlayLists.list_playlist});
             }
             debugger
        if(nextProps.id!==undefined) {
            this.setState({song_id:nextProps.id});
        }
        if(nextProps.message!==undefined && this.state.toast==false){
            this.setState({toast:true});
              this.props.dispatch(userActions.getPlayLists());
                 ToastAndroid.show('deleted successfully',ToastAndroid.SHORT);
        }
        if(nextProps.messages!==undefined && this.state.navigate==false){
            this.setState({navigate:true},()=>{
                this.props.navigation.navigate('PlaylistSongs', {'id': this.state.playlistid});
            })

        }
    }
    render(){
        return(

                <View style={{flex:1,backgroundColor:'black'}}>
                    { this.state.loading? <View style={{flex:1,justifyContent:'center'}}>
                            <ActivityIndicator size="large"/>


                        </View>:
                        <View style={{flex:1}}>

                    <View style={{height:40,backgroundColor:'#000',width:'100%'}}>
                        <View style={{flex:1,flexDirection:'row',marginHorizontal:10,justifyContent:'space-between'}}>
                        <Text style={{alignSelf:'center',color:'#fff',fontSize:16,marginTop:10,marginBottom:10,justifyContent:'flex-start'}}>Create New PlayList</Text>
                            <TouchableWithoutFeedback onPress={()=>this.onAddPlaylist()}>
                            <View>
                        <Text style={{alignSelf:'center',color:'#f47b21',fontSize:16,marginTop:10,marginBottom:10,justifyContent:'flex-end'}}>Create</Text>
                            </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>

                    {this.state.add &&
                        <View style={{flexDirection:'row'}}>
                    <TextInput style={{flex:1,color:'white',marginLeft:8,marginTop:8}} label={'name'} placeholder={'Enter Playlist Name'} placeholderTextColor="white" value={this.state.newlist} onChangeText={(text) => this.setState({newlist: text})}/>
                       <Button buttonStyle={{marginRight:8,marginTop:8}} transparent title={'Create'} color="#0295f7" onPress={()=>{this.addnew()}}/>
                        </View>
                    }

                    <View style={{flex:1,alignItems:'flex-start',justifyContent:'center',width:'100%'}}>
                        <View style={{backgroundColor:'#2f3032',height:40,width:'100%'}}>
                    <Text style={{alignSelf:'center',color:'#f47b21',fontSize:16,marginTop:10,marginBottom:10,justifyContent:'center'}}>My PlayLists</Text>

                </View>
                        <ScrollView>
                            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                                <Modal
                                    animationType="slide"
                                    transparent={true}
                                    visible={this.state.visible}
                                    onRequestClose={() => {
                                        this.setState({visible: false});
                                    }}>
                                    <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'rgba(0,0,0,0.9)'}}>
                            <ListPopover
                                list={items}
                                isVisible={this.state.visible}
                                onClick={(item) => {
                                    this.setState({item: item}), this.clickPopover(item) } }
                                onClose={() => this.setState({visible: false})}/>
                                    </View>
                                </Modal>
                            </View>
                            {this.state.userPlayLists && this.state.userPlayLists.map((item, index) => (
                                <View key={item.i_playlist}>
                            <MyplaylistList i_playlist={item.i_playlist} playlist_name={item.playlist_name} playListAction={this.playListAction} delete={this.onlongPress} />
                                </View>


                            ))

                            }
                        </ScrollView>
                    </View>

                        </View> }
                        </View>

        );
    }
}
const mapStateToProps=(state)=>{
    return {
        userPlayLists:state.dashboard.userPlayLists,
        created:state.dashboard.created,
        id:state.dashboard.id,
        message:state.dashboard.message,
        messages:state.dashboard.messages
    }
}
export default connect(mapStateToProps)(Myplaylist);