import React, { Component } from 'react';
import {Text,StyleSheet,ToastAndroid,View,Animated,PanResponder} from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button,Toast } from 'native-base';
import {Icon} from 'react-native-elements';
import {NavigationActions} from 'react-navigation';
import PlayerComponent from '../Components/PlayerComponent';
import {connect} from 'react-redux';
import _ from 'lodash';
class PlayerContainer extends Component {
    constructor(props){
        super(props);
        this.state={
            currentTab:1,
            showToast: false,
            player:false,
            pan: new Animated.ValueXY()

        }

    }
    componentWillMount(){
        const touchThreshold = 20;
        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => false,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => false,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponder : (e, gestureState) => {
                const {dx, dy} = gestureState;

                return (Math.abs(dx) > touchThreshold) || (Math.abs(dy) > touchThreshold);
            },
            onPanResponderGrant: () => this.setState({scroll: false}),
            onPanResponderMove: Animated.event([null, {dx: this.state.pan.x, dy: this.state.pan.y}]),
            onPanResponderRelease:  (e, gesture)  => {
                Animated.spring(this.state.pan, {
                    toValue: { x: 0, y: 0 },

                }).start()
                this.props.navigation.navigate('Player');
            }

        })
    }

    componentWillReceiveProps(nextProps){
        debugger
        if(!_.isEmpty(nextProps.currentPlayingAlbum)||nextProps.isPlaying===true){
            this.setState({player:true});

        }

    }

    render() {
        return (

            <View style={{backgroundColor:'#242424'}}>
                <View style={this.state.player?{height:90,backgroundColor:'#242424'}:{height: 0, opacity: 0}}>
                    <PlayerComponent showWallet={this.showWallet}/>
                </View>
            </View>

        );
    }
}

const styles=StyleSheet.create({
    IconsCol:{
        color:'#fff',
        borderBottomWidth:4,
        borderBottomColor:'#fff',


    },
    background:{
        // borderBottomWidth:4,
        // borderBottomColor:'#fff',
        backgroundColor:'transparent',
        borderBottomWidth:6,
        borderBottomColor:'#ec223f'

    },
    border:{
        borderBottomWidth:2,
        borderBottomColor:'#ec223f'
    }

});

const mapStateToProps=(state)=>{
    return{
        isPlaying:state.player.isPlaying,
        currentPlayingAlbum:state.player.currentPlayingAlbum
    }
}
export default connect(mapStateToProps)(PlayerContainer);
