import React,{Component} from 'react';
import {View, Text, Image, ScrollView, StyleSheet,Dimensions} from 'react-native';
import { List, ListItem } from 'react-native-elements'



export default class Playerview extends Component {
    constructor(props){
        super(props);
        this.state={
            list: [
                {
                    name: 'Amy Farha',
                    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
                    subtitle: 'Vice President'
                },
                {
                    name: 'Chris Jackson',
                    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
                    subtitle: 'Vice Chairman'
                },

            ]
        }
    }
    render(){
        return(

            <View style={{flex:1,backgroundColor:'black'}}>
               <View style={{justifyContent:'center',alignItems:'center',marginTop:14}}>
               <Image style={Styles.imgcenter} source={{uri:'https://images.pexels.com/photos/248797/pexels-photo-248797.jpeg?h=350&auto=compress&cs=tinysrgb'}}/>
               </View>
                <View>
                    <ScrollView>
                    <List containerStyle={{marginBottom:10}}>
                        {
                            this.state.list.map((l, i) => (
                                <ListItem
                                    roundAvatar
                                    avatar={{uri:'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg'}}
                                    key={i}
                                    title={l.name}
                                    hideChevron={true}
                                />
                            ))
                        }
                    </List>
                    </ScrollView>
                </View>
            </View>

        );
    }
}

const Styles=StyleSheet.create({
    imgcenter: {
        width: 120,
        height: 120,
        borderRadius: 60


    },
});