import React ,{Component} from 'react';
import {View, TextInput,Text, Image, ImageBackground, TouchableOpacity, StyleSheet,ScrollView,ToastAndroid,ActivityIndicator} from 'react-native';
import {Avatar,Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Header} from 'react-native-elements';
import HeaderComponent from '../Components/HeaderComponent';
import FooterComponent from '../Components/FooterComponent';
import ImagePicker from 'react-native-image-crop-picker';
import {connect} from 'react-redux';
import {userActions} from "../store/actions/dashboard";
import imageCacheHoc from 'react-native-image-cache-hoc';

class Profile extends  Component{
    constructor(props){
        super(props);
        this.state={
            file:false,
            firstName:'',
            lastName:'',
            newpass:'',
            band:'',
            change:'',
            error:'',
            current:'',
            confirm:'',
            ig:'',
            fblink:'',
            twitterlink:'',
            slack:'',
            reddit:'',
            youtube:'',
            telegram:'',
            password:false,
            message:false,
            soundCloud:false,
            youtubes:false,
            reddits:false,
            twitter:false,
            facebook:false,
            insta:false,
            igActive:false,
            fbActive:false,
            twActive:false,
            slActive:false,
            ytActive:false,
            rdActive:false,
            proimgLoad:false



        }
    }
    changePassword=()=>{
        debugger
    if(this.state.change!==this.state.confirm&&this.state.change!==''&&this.state.confirm!==''){
            this.setState({error:"Password Mismatch"});
        }else {
        let data={
            current:this.state.current,
            change_pass:this.state.confirm,
        }
        let dataa=JSON.stringify(data);
        this.props.dispatch(userActions.changePass(dataa));
    }

    }
    toggleState=(name)=>{
        debugger
       this.setState({...name,...this.state});

    }
    updateProfile=()=>{

            let data={
                firstname:this.state.firstName,
                lastname:this.state.lastName,
                email:this.state.email,
                bandname:this.state.band,
                instagram_link:this.state.ig,
                facebook_link:this.state.fblink,
                twitter_link:this.state.twitterlink,
                youtube_link:this.state.youtube,
                slack_link:this.state.slack,
                reddit_link:this.state.reddit,
                telegram_link:this.state.telegram }

            let dataa=JSON.stringify(data);
            this.props.dispatch(userActions.updateProfile(dataa));






        }


    changePic=()=>{
        this.setState({proimgLoad:true});
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true,
            mediaType: 'photo'
        }).then(
            image => {
                this.setState({pic: image.path, file: image});
                this.state.file.uri=this.state.pic;
                const formData = new FormData()
                formData.append('image', {uri:this.state.file.uri,name:this.state.pic,type:'multipart/form-data'});

                this.props.dispatch(userActions.updateProfileImage(formData));

            }

        )
    }
    componentWillMount(){
     this.props.dispatch(userActions.getdetails());
    }
    componentWillReceiveProps(nextProps){
        debugger
         if(nextProps.passwordmessage&&this.state.message===false){
            this.setState({message:true});
             ToastAndroid.show(nextProps.passwordmessage,ToastAndroid.SHORT);
             this.props.dispatch(userActions.getdetails());
             this.setState({password:false});
         }
        if(nextProps.url){

            this.props.dispatch(userActions.getdetails());
            this.setState({proimgLoad:false});
           // this.state.myprofile.profile_url=nextProps.url;
            // this.setState({image:nextProps.url});
        }
        if(nextProps.status==200){
            this.props.dispatch(userActions.getdetails());
        }
        this.setState({status:nextProps.status});
        this.setState({myprofile:nextProps.userdetails.myprofile[0]});
        this.setState({email:nextProps.userdetails.myprofile[0].email});
        this.setState({firstName:nextProps.userdetails.myprofile[0].firstname});
        this.setState({lastName:nextProps.userdetails.myprofile[0].lastname});
        this.setState({image:nextProps.userdetails.myprofile[0].profile_url});
        this.setState({band:nextProps.userdetails.myprofile[0].bandname});
        this.setState({ig:nextProps.userdetails.myprofile[0].instagram_link});
        this.setState({fblink:nextProps.userdetails.myprofile[0].facebook_link});
        this.setState({twitterlink:nextProps.userdetails.myprofile[0].twitter_link});
        this.setState({reddit:nextProps.userdetails.myprofile[0].reddit_link});
        this.setState({telegram:nextProps.userdetails.myprofile[0].telegram_link});
        this.setState({youtube:nextProps.userdetails.myprofile[0].youtube_link});
        this.setState({slack:nextProps.userdetails.myprofile[0].slack_link});


    }
    componentDidMount(){

    }

  render(){
      return(
          this.state.password?<View style={{backgroundColor:'#000',flex:1}}>
              <View style={{flex:1,alignItems:'center'}}>
                  <TouchableOpacity onPress={()=>this.setState({password:false})}>
                      <View style={{alignItems:'flex-end'}}>
                          <Text style={{paddingTop:20,fontFamily:'Ubuntu-R',color:'#0295f7'}}>
                              Other-Settings
                          </Text>
                      </View>
                  </TouchableOpacity>
                  <Text style={{color:'red'}}>{this.state.error && this.state.error}</Text>
                  <View style={styles.icons}>
                      <TextInput style={{flex:1,color:'white'}} placeholder={'Current Password'} value={this.state.current} onChangeText={(text)=>{this.setState({current:text})}} placeholderTextColor="white"/>

                  </View>

                  <View style={styles.icons}>
                      <TextInput style={{flex:1,color:'white'}} placeholder={'Change Password'} value={this.state.change} onChangeText={(text)=>{this.setState({change:text})}} placeholderTextColor="white"/>
                  </View>
                  <View style={styles.icons}>

                      <TextInput style={{flex:1,color:'white'}} placeholder={'Confirm Password'} value={this.state.confirm} onChangeText={(text)=>{this.setState({confirm:text})}} placeholderTextColor="white"/>
                  </View>
                  <View style={{marginTop:10,alignSelf:'center'}}>
                      <TouchableOpacity onPress={this.changePassword}>
                          <View style={{borderRadius:80,alignItems:'center',justifyContent:'center'}}>
                              <ImageBackground  style={{paddingHorizontal:20,paddingVertical:7,marginBottom:8,height:40}} source={require('../images/button.png')}>
                                  <Text style={styles.btntext}>Change Password</Text>
                              </ImageBackground>
                          </View>
                      </TouchableOpacity>
                      </View>
              </View>

              </View>:

              <View style={{flex:1,backgroundColor:'#000'}}>
            <ScrollView>

                      <View style={{alignSelf:'center'}}>

                          {this.state.myprofile &&
                         !this.state.proimgLoad? <View key={this.state.myprofile.profile_url}>
                              <Avatar
                                  xlarge
                                  rounded
                                  key={this.state.myprofile.profile_url}
                                  source={{uri: this.state.myprofile.profile_url + '?' + new Date()}}
                                  containerStyle={{
                                      width: 160,
                                      height: 160,
                                      borderRadius: 50
                                  }}
                              >

                              </Avatar>

                              <Avatar
                                  small
                                  rounded
                                  overlayContainerStyle={{backgroundColor: '#c0c0c0'}}
                                  icon={{name: 'camera', color: 'white'}}
                                  onPress={this.changePic}

                                  containerStyle={{zIndex: 999999, top: 120, right: 22, position: 'absolute'}}
                              />
                          </View>:
                              <View style={{ backgroundColor: '#000',  width: 160,
                                  height: 160,
                                  borderRadius: 80,justifyContent:'center'}}>
                                  <ActivityIndicator size="large"/>
                             </View>
                          }

                          </View>

                {/*<TouchableOpacity onPress={()=>this.setState({password:true})}>*/}
                    {/*<View style={{alignItems:'flex-end'}}>*/}
                        {/*<Text style={{paddingTop:20,fontFamily:'Ubuntu-R',color:'#0295f7'}}>*/}
                            {/*Change-Password*/}
                        {/*</Text>*/}
                    {/*</View>*/}
                {/*</TouchableOpacity>*/}


                  <Text style={{color:'red'}}>{this.state.error && this.state.error}</Text>
                   <View style={{flex:1}}>
                       <View style={styles.icons}>
                           <Image source={require('../logos/ic-user.png')} style={styles.ImageStyle} />
                           <TextInput  selectTextOnFocus={false} style={{flex:1,color:'white'}} label={'Email'} value={this.state.firstName}  placeholder={'First name'} placeholderTextColor="white" onChangeText={(text) => this.setState({firstName: text})}
                           />
                       </View>
                       <View style={styles.icons}>
                           <Image source={require('../logos/ic-user.png')} style={styles.ImageStyle} />
                           <TextInput selectTextOnFocus={false} style={{flex:1,color:'white'}} label={'First name'} value={this.state.lastName}  placeholder={'Last name'} placeholderTextColor="white" onChangeText={(text) => this.setState({lastName: text})}
                           />
                       </View>
                       <View style={styles.icons}>
                           <Image source={require('../logos/ic-mail.png')} style={styles.ImageStyle} />
                           <TextInput editable={false} selectTextOnFocus={false} style={{flex:1,color:'white'}} label={'Last Name'} value={this.state.email}  placeholder={'Email'} placeholderTextColor="white" onChangeText={(text) => this.setState({email: text})}
                           />
                       </View>
                      <View style={styles.icons}>
                          <Image source={require('../logos/ic-drum.png')} style={styles.ImageStyle} />
                          <TextInput style={{flex:1,color:'white'}} placeholder={'Enter Band name'} value={this.state.band} onChangeText={(text)=>{this.setState({band:text})}} placeholderTextColor="white" />
                      </View>
                       <View style={{alignItems:'center'}}>
                           <Text style={{marginBottom:16,paddingTop:20,fontFamily:'Ubuntu-R',color:'#fff'}}>
                              Connect To Social Networks
                           </Text>
                       </View>

                       <View style={{flexDirection:'row',justifyContent:'space-around'}}>
                           <Icon name="instagram" size={26} color={this.state.insta?'#ec223f':'#fff'} onPress={()=>this.setState({insta:!this.state.insta})}/>
                           <Icon name="facebook-official" size={26} color={this.state.facebook?'#ec223f':'#fff'} onPress={()=>this.setState({facebook:!this.state.facebook})} />
                           <Icon name="twitter" size={26} color={this.state.twitter?'#ec223f':'#fff'} onPress={()=>this.setState({twitter:!this.state.twitter})}/>
                           <Icon name="youtube" size={26} color={this.state.youtubes?'#ec223f':'#fff'} onPress={()=>this.setState({youtubes:!this.state.youtubes})}/>
                           <Icon name="reddit" size={26} color={this.state.reddits?'#ec223f':'#fff'} onPress={()=>this.setState({reddits:!this.state.reddits})}/>
                           <Icon name="soundcloud" size={26} color={this.state.soundCloud?'#ec223f':'#fff'} onPress={()=>this.setState({soundCloud:!this.state.soundCloud})}/>

                       </View>

                       {this.state.insta &&  <View style={styles.icons}>

                        <TextInput style={{flex:1,color:'white',borderBottomWidth:this.state.igActive?1:0,borderBottomColor:this.state.igActive?'ec223f':
                                '#fff'}} placeholder={'Enter instagram-link'} underlineColorAndroid='transparent' value={this.state.ig} onFocus={()=>{this.setState({igActive:true})}} onBlur={()=>{this.setState({igActive:false})}} onChangeText={(text)=>{this.setState({ig:text})}} placeholderTextColor="white" />
                    </View>}
                       {this.state.facebook &&  <View style={styles.icons}>

                        <TextInput style={{flex:1,color:'white',borderBottomWidth:this.state.fbActive?1:0,borderBottomColor:this.state.fbActive?'#ec223f':
                                '#fff'}} placeholder={'Enter Facebook-link'} underlineColorAndroid='transparent' value={this.state.fblink} onFocus={()=>{this.setState({fbActive:true})}} onBlur={()=>{this.setState({fbActive:false})}} onChangeText={(text)=>{this.setState({fblink:text})}} placeholderTextColor="white" />
                    </View>}
                       {this.state.twitter &&  <View style={styles.icons}>

                        <TextInput style={{flex:1,color:'white',borderBottomWidth:this.state.twActive?1:0,borderBottomColor:this.state.twActive?'#ec223f':
                                '#fff'}} placeholder={'Enter twitter-link'} underlineColorAndroid='transparent' value={this.state.twitterlink} onFocus={()=>{this.setState({twActive:true})}} onBlur={()=>{this.setState({twActive:false})}}  onChangeText={(text)=>{this.setState({twitterlink:text})}} placeholderTextColor="white" />
                    </View> }

                       {this.state.youtubes && <View style={styles.icons}>

                        <TextInput style={{flex:1,color:'white',borderBottomWidth:this.state.ytActive?1:0,borderBottomColor:this.state.ytActive?'#ec223f':
                                '#fff'}} placeholder={'Enter youtube-link'} value={this.state.youtube} onFocus={()=>{this.setState({ytActive:true})}} onBlur={()=>{this.setState({ytActive:false})}} onChangeText={(text)=>{this.setState({youtube:text})}} placeholderTextColor="white"/>
                    </View> }
                    {/*<View style={styles.icons}>*/}
                        {/*<TextInput style={{flex:1,color:'white'}} placeholder={'Enter Telegram-link'} value={this.state.telegram} onChangeText={(text)=>{this.setState({telegram:text})}} placeholderTextColor="white" />*/}
                    {/*</View>*/}
                       {this.state.reddits &&  <View style={styles.icons}>

                        <TextInput style={{flex:1,color:'white',borderBottomWidth:this.state.rdActive?1:0,borderBottomColor:this.state.rdActive?'#ec223f':
                            '#fff'}} placeholder={'Enter reddit-link'} underlineColorAndroid='transparent' value={this.state.reddit} onFocus={()=>{this.setState({rdActive:true})}} onBlur={()=>{this.setState({rdActive:false})}} onChangeText={(text)=>{this.setState({reddit:text})}} placeholderTextColor="white" />
                    </View> }
                       {this.state.soundCloud &&  <View style={styles.icons}>

                        <TextInput style={{flex:1,color:'white',borderBottomWidth:this.state.slActive?1:0,borderBottomColor:this.state.slActive?'#ec223f':
                        '#fff'}} placeholder={'Enter slack-link'} underlineColorAndroid='transparent' value={this.state.slack} onFocus={()=>{this.setState({slActive:true})}} onBlur={()=>{this.setState({slActive:false})}} onChangeText={(text)=>{this.setState({slack:text})}} placeholderTextColor="white"/>
                    </View> }
                    <View style={{flex:1,flexDirection:'row',justifyContent:'center'}}>
                      <View style={{marginTop:10,marginBottom:16,alignSelf:'center'}}>
                          <TouchableOpacity onPress={this.updateProfile}>
                          <View style={{borderRadius:80}}>
                              <ImageBackground  style={{paddingHorizontal:42,marginTop:8,marginBottom:8,height:30}} source={require('../images/button.png')}>
                                  <Text style={styles.btntext}>Update</Text>
                              </ImageBackground>
                          </View>
                      </TouchableOpacity>
                      </View>



                   </View>
                       <View style={{marginBottom:16,alignSelf:'center'}}>
                           <TouchableOpacity onPress={()=>this.setState({password:true})}>
                               <View style={{borderRadius:80,alignItems:'center',justifyContent:'center'}}>
                                   <ImageBackground  style={{paddingHorizontal:16,paddingVertical:7,marginBottom:8,height:40}} source={require('../images/button.png')}>
                                       <Text style={styles.btntext}>Password Settings</Text>
                                   </ImageBackground>
                               </View>
                           </TouchableOpacity>
                       </View>
                   </View>




            </ScrollView>





          </View>



          );
  }

}
const styles=StyleSheet.create({
    btntext:{
        lineHeight:10,
        fontSize:15,
        marginHorizontal:24,
        paddingBottom:10,
        color:'#f5f5f5',
        fontFamily:'Ubuntu-R',
        textAlign: 'center',
        alignItems:'center',
        justifyContent:'center'

    },
    center:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#ffffff',


    },
    text:{
        fontSize:20,
        color:'#ffffff',
        marginBottom:20,
        height:50,
        backgroundColor: 'transparent',
        fontFamily:'Ubuntu-R',
        textAlign: 'center',
        alignItems:'center',
        justifyContent:'center'

    },
    input:{
        // fontSize:15,
        // color:'#8E30A5'
        // borderRadius:2,
        //
        justifyContent:'center',
        alignSelf:'center',
        // borderColor:'#8E30A5',
        marginTop:180,
        flex:1,
        width:'90%',



    },
    button:{
        backgroundColor:'#0295f7',
        alignSelf:'center',
        width:'50%',
        justifyContent:'center',
        height:40,
        overflow:'hidden',
        borderRadius:4,


    },
    btntext:{
        fontSize:20,
        color:'#ffffff',
        fontFamily:'Ubuntu-R',
        textAlign: 'center',
        alignItems:'center',
        justifyContent:'center'

    },
    icons:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        height: 40,
        borderRadius: 5 ,
        margin: 10
    },
    ImageStyle: {
        padding: 10,
        margin: 5,
        height: 20,
        width: 20,
        resizeMode : 'stretch',
        alignItems: 'center'
    },
    row:{
        flex: 1,
        flexDirection: "row"
    },
    header:{
        height:55,
        backgroundColor:'#242424',
        justifyContent:'center'
    },
    inner:{
        justifyContent:'center'
    },
    overlays:{


        flexDirection:'row',
        backgroundColor:'#000',


    },
    overlay:{
        backgroundColor:'#fff',

    }

});

const mapStateToProps=(state)=>{
    return {
        userdetails: state.dashboard.details,
        status:state.dashboard.status,
        url:state.dashboard.url,
        passwordmessage:state.dashboard.passwordmessage
    }
}

export default connect(mapStateToProps)(Profile);