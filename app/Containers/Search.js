import React,{Component} from 'react';
import {View,Text,ScrollView,Image,StyleSheet,BackHandler} from 'react-native';
import {userActions} from "../store/actions/dashboard";
import {connect} from 'react-redux';
import { List, ListItem } from 'react-native-elements'
import {playerAction} from "../store/actions/playerAction";


class Search extends Component{
    constructor(props){
        super(props);
        this.state={
            searchdata:[],
            songList:[],
            albumlist:[],
            artist:[]
        }
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }



    handleBackButtonClick = () => {
        debugger
        this.props.navigation.goBack(null);
        return true;
    };
    play=(id,url,imageUrl,songName)=>{
        let params={id:id,url:url,imageUrl:imageUrl,songName:songName};
        this.props.dispatch(playerAction.playSong(params));

    }
    search=(text)=>{
        debugger
        if(text.length>=3) {
            this.props.dispatch(userActions.searchdata(text));
        }
    }
    componentWillReceiveProps(nextProps){
             debugger
            this.setState({albumlist:nextProps.searchdata.res.albums});
            this.setState({songList:nextProps.searchdata.res.songs});
            this.setState({artist:nextProps.searchdata.res.artist});

    }
    componentWillUnmount(){
        debugger
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        this.props.dispatch(userActions.emptySearch());

    }

    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <ScrollView>
                {this.state.albumlist && <View style={{height:32,marginTop:8,backgroundColor:'#2f3032',alignItems:'center',justifyContent:'center'}}>
                    <Text style={{marginHorizontal:5,color:'#f47b21',fontFamily:'Ubuntu-R'}}>Albums</Text>

                </View> }
               <List containerStyle={{backgroundColor:'transparent',borderBottomColor: 'transparent',borderTopColor:'transparent'}}>

                    { this.state.albumlist.map((l, i) => (
                            <ListItem
                                containerStyle={{ borderBottomColor: 'transparent' }}
                                titleStyle={{color:'#fff'}}
                                subtitleStyle={{color:'#fff'}}
                                roundAvatar
                                avatar={{uri:l.album_cover}}
                                key={i}
                                title={l.album_name}
                                hideChevron={true}
                                onPress={()=> {this.props.dispatch(userActions.emptySearch()); this.props.navigation.navigate('Album',{'id':l.i_album})}}
                            />
                        ))
                    }
                </List>

                {this.state.songList &&
                <View style={{height:32,marginTop:8,backgroundColor:'#2f3032',alignItems:'center',justifyContent:'center'}}>
                <Text style={{marginHorizontal:5,color:'#f47b21',fontFamily:'Ubuntu-R'}}>Songs</Text>
                </View>
                }
                <List containerStyle={{backgroundColor:'transparent',borderBottomColor: 'transparent',borderTopColor:'transparent'}}>

                    { this.state.songList.map((l, i) => (
                        <ListItem
                            containerStyle={{ borderBottomColor: 'transparent' }}
                            titleStyle={{color:'#fff'}}
                            subtitleStyle={{color:'#fff'}}
                            roundAvatar
                            avatar={{uri:l.album_cover}}
                            key={i}
                            title={l.song_name}
                            subtitle={l.album_name}
                            hideChevron={true}
                            url={l.song_link}
                            id={l.i_song}
                            onPress={()=>this.play(l.i_song,l.song_link,l.album_cover,l.song_name)}
                        />
                    ))
                    }
                </List>
                {this.state.artist &&
                <View style={{height:32,marginTop:8,backgroundColor:'#2f3032',alignItems:'center',justifyContent:'center'}}>
                <Text style={{marginHorizontal:5,color:'#f47b21',fontFamily:'Ubuntu-R'}}>Artist</Text>
                </View>}
                <List containerStyle={{backgroundColor:'transparent',borderBottomColor: 'transparent',borderTopColor:'transparent'}}>

                    { this.state.artist.map((l, i) => (
                        <ListItem
                            containerStyle={{ borderBottomColor: 'transparent' }}
                            titleStyle={{color:'#fff'}}
                            subtitleStyle={{color:'#fff'}}
                            roundAvatar
                            avatar={{uri:l.profile_url}}
                            key={i}
                            title={l.artist_name}
                            hideChevron={true}
                        />
                    ))
                    }
                </List>
                </ScrollView>
            </View>
        )
    }


}
const Styles=StyleSheet.create({
    ImageStyle: {
        padding: 10,
        marginHorizontal: 5,
        height: 20,
        width: 20,
        resizeMode : 'stretch',
        alignItems: 'center'
    },
});

const mapStateToProps=(state)=>{
     return {
         searchdata:state.dashboard.searchdata
     }
}


export default connect(mapStateToProps)(Search);