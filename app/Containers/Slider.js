
import React, { Component } from 'react'
import {
    Text,
    View,
    Image,
    Dimensions
} from 'react-native'
import Swiper from 'react-native-swiper';
import {userActions} from "../store/actions/dashboard";
import {connect} from 'react-redux';
const { width } = Dimensions.get('window');

const styles = {
    wrapper: {
    },

    slide: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'transparent'
    },
    image: {
        width,
        flex: 1,
        backgroundColor: 'transparent'
    },

    loadingView: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        backgroundColor: 'rgba(0,0,0,.5)'
    },


}

const Slide = props => {
    return (<View style={styles.slide}>
        <Image resizeMode='cover' onLoad={props.loadHandle.bind(null, props.i)} style={styles.image} source={{uri: props.uri}} />
    </View>)
}

 class TopScreen extends Component {
    constructor (props) {
        super(props)
        this.state = {
            imgList:[],
            loadQueue: []
        }

        this.loadHandle = this.loadHandle.bind(this)
    }
    componentWillMount(){
        this.props.dispatch(userActions.slider());
    }
    componentWillReceiveProps(nextProps){

        this.setState({imgList:nextProps.imgList});
        this.setState({loadQueue:nextProps.load});
    }
    loadHandle (i) {


        let loadQueue = this.state.loadQueue
        loadQueue[i] = 1
        this.setState({
            loadQueue
        })
    }
    render () {
        return (
            <View style={{flex: 1}}>
                <Swiper loop={true} style={styles.wrapper} autoplay={true} >
                    {this.state.imgList && this.state.imgList.map((item, i) => <Slide
                            loadHandle={this.loadHandle}
                            loaded={!!this.state.loadQueue[i]}
                            uri={item.img_url}
                            i={i}
                            key={i} />)
                    }
                </Swiper>
            </View>
        )
    }
}
const mapStateToProps=(state)=>{
    return {
        imgList:state.dashboard.imgList,
        load:state.dashboard.load
    }
}

export default connect(mapStateToProps)(TopScreen);
