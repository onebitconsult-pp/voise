import React , {Component} from 'react';
import {View,Text,ImageBackground,Image,StyleSheet,ScrollView,TouchableWithoutFeedback} from 'react-native';
import {ButtonGroup,Icon} from 'react-native-elements';
import WithdrawlContainer from './WithdrawlContainer';
import DepositContainer from './DepositContainer';
import LinearGradient from 'react-native-linear-gradient';

import {userActions} from "../store/actions/dashboard";
import {connect} from 'react-redux';
import { Clipboard } from 'react-native';
class WalletContainer extends Component {
    constructor () {
        super()
        this.state = {
            selectedIndex: 1,
            withdraw:undefined,
            deposit:undefined,
            details:undefined,
            withdrawals:true,
            deposits:false,
            addr:''
        }

    }
    componentWillMount(){
        debugger
        this.props.dispatch(userActions.getWallet());
    }

    componentWillReceiveProps(nextProps){
    debugger
        this.setState({details:nextProps.details.wallet_details[0]});
        this.setState({deposit:nextProps.details.deposit});
        this.setState({withdraw:nextProps.details.withdraw});
        this.setState({addr:nextProps.details.wallet_details[0].eth_wallet});
    }
    writeToClipboard = async () => {
        await Clipboard.setString(this.state.details.eth_wallet);
        alert('Copied to Clipboard!');
    };

    render () {

        const component2 = () => selectedIndex===1?<Text>Deposit</Text>: <TouchableWithoutFeedback >
            <View style={styles.button}  resizeMode="cover">
                <Text style={styles.btntext,{borderBottomWidth:2,borderBottomColor:'#ec223f'}}>Deposit</Text>
                {/*<ImageBackground  style={{paddingLeft:30,paddingRight:30,paddingTop:10,paddingBottom:15}} source={require('../images/button.png')}><Text style={styles.btntext}>Deposit</Text>*/}
                {/*</ImageBackground>*/}
            </View>

        </TouchableWithoutFeedback>;
        const component3 = () => selectedIndex===0?<Text>Withdrawal</Text>: <TouchableWithoutFeedback >
            <View style={styles.button}  resizeMode="cover">
                <Text style={styles.btntext,{borderBottomWidth:2,borderBottomColor:'#ec223f'}}>withdraw</Text>
                {/*<ImageBackground  style={{paddingLeft:26,paddingRight:26,paddingTop:10,paddingBottom:15}} source={require('../images/button.png')}><Text style={styles.btntext}>withdraw</Text>*/}
                {/*</ImageBackground>*/}
            </View>

        </TouchableWithoutFeedback>
        const buttons = [ { element: component2 }, { element: component3 }]

        return (
            <View style={{flex:1, backgroundColor: '#000'}}>
                <ImageBackground
                    style={{
                        backgroundColor: '#000',
                        flex: 1,
                        width: '100%',
                        height: '100%',

                    }}
                    source={require('../images/bg_image.png')}>
                    {/*<View style={{marginTop:8,flexDirection:'row',width:'100%'}}>*/}
                        {/*<View style={{alignItems:'flex-start',justifyContent:'flex-start',marginLeft:20}}>*/}
                        {/*<Icon name='wallet' color='white' type='entypo' style={{color:'#fff'}}/>*/}
                        {/*</View>*/}
                    {/*<View style={{alignSelf:'flex-end',justifyContent:'center',marginLeft:20}}>*/}
                            {/*<Icon name='copy' color='white' type='entypo' onPress={this.writeToClipboard} style={{justifyContent:'flex-end'}}/>*/}
                    {/*</View>*/}
                   {/*</View>*/}
                    <View style={{height:32,justifyContent:'center'}}>
                    <LinearGradient colors={['#f17240' , '#e71881', '#000000']} style={styles.linearGradient}   start={{ x: 0, y: 1 }}
                                    end={{ x: 1, y: 1 }}>
                        <Text style={styles.buttonText,{alignSelf:'flex-start',marginTop:6,textAlign: 'center',
                            color: '#fff'}}>
                            YOUR BALANCE
                        </Text>
                    </LinearGradient>
                    </View>
                    <View style={{height:110,justifyContent:'space-around',marginTop:24}}>
                        <View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:16}}>
                            <Text style={{justifyContent:'space-between',color:'#ec223f',fontSize:18}}>ETH</Text>
                            <Text style={{justifyContent:'space-between',color:'#ec223f',fontSize:18}}>{this.state.details && this.state.details.eth_balance}</Text>

                        </View>

                    <View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:16}}>
                        <Text style={{justifyContent:'space-between',color:'#ec223f',fontSize:24}}>VOISE</Text>
                        <Text style={{justifyContent:'space-between',color:'#ec223f',fontSize:24}}>{this.state.details && this.state.details.voise_balance}</Text>

                    </View>


                    </View>

                    <View style={{height:110,backgroundColor:'#242424'}}>
                        <Text style={{color:'#ec223f',marginLeft:16}}>YOUR ADDRESS</Text>
                        <Text style={{color:'#fff',marginLeft:16}}>{this.state.details && this.state.details.eth_wallet}</Text>
                        <View style={{alignSelf:'flex-start',justifyContent:'flex-end',marginVertical:16,marginLeft:16}}>
                            <TouchableWithoutFeedback onPress={this.writeToClipboard}>
                                <View style={{flexDirection:'row',marginHorizontal:8}}>
                        <Icon name='copy' color='white' type='entypo'  style={{justifyContent:'flex-end',marginRight:8}}/>
                                <Text style={{color:'#ec223f',marginLeft:8}}>COPY</Text>
                                </View>
                            </TouchableWithoutFeedback>

                        </View>

                    </View>

                    {/*<View>*/}
                        {/*<Text style={{color:'#fff', paddingHorizontal:8, paddingVertical: 12}}>*/}
                            {/**/}
                        {/*</Text>*/}
                    {/*</View>*/}

                        {/*<View style={{alignItems:'center',justifyContent:'center',width:'100%'}}>*/}
                            {/*<View style={{justifyContent:'flex-start',alignItems:'flex-start'}}>*/}
                            {/*<Text style={{color:'#fff',alignSelf:'center',fontSize:18}}>Eth</Text>*/}
                            {/*<Text style={{color:'#fff',fontSize:18,fontWeight:'bold'}}></Text>*/}
                            {/*</View>*/}
                            {/*<View style={{justifyContent:'flex-end',alignItems:'flex-end',marginTop:4}}>*/}
                            {/*<Text style={{color:'#fff',fontSize:18}}>Voise Balance</Text>*/}
                            {/*<Text style={{color:'#fff',alignSelf:'center',fontSize:18,fontWeight:'bold'}}>{this.state.details && this.state.details.voise_balance}</Text>*/}
                            {/*</View>*/}
                            {/*</View>*/}




                </ImageBackground>

                    <View style={{alignItems:'center',backgroundColor:'#202020'}}>

                        <View style={styles.button} >
                            { this.state.withdrawals  ?
                                <View style={{borderRadius:60,marginHoizontal:8,marginRight:16}}>
                                     <Text style={styles.btntext,{fontWeight:'bold',color:'#fff',borderBottomWidth:2,borderBottomColor:'#ec223f'}}>Withdrawal</Text>

                                </View>
                                :
	                            <View style={{borderRadius:60,marginHoizontal:8,marginRight:16}}>
                                    <TouchableWithoutFeedback onPress={()=>{this.setState({withdrawals:true,deposits:false})}} >
                                          <View>
                                            <Text style={styles.btntext,{color:'#fff',borderBottomWidth:2,borderBottomColor:'transparent'}}>Withdrawal</Text>
                                          </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            }
                            { this.state.deposits ?
                                <View style={{borderRadius:60,marginHoizontal:8,marginRight:16}}>
                                    <Text style={styles.btntext,{fontWeight:'bold',color:'#fff',borderBottomWidth:2,borderBottomColor:'#ec223f'}}>Deposit</Text>

                                </View>
                                :
	                            <View style={{borderRadius:60,marginHoizontal:8,marginRight:16}}>
                                    <TouchableWithoutFeedback onPress={()=>{this.setState({withdrawals:false,deposits:true})}}>
                                           <View>
                                            <Text style={styles.btntext,{color:'#fff',borderBottomWidth:2,borderBottomColor:'transparent'}}>Deposit</Text>
                                           </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            }
                        </View>






                    </View>

                    <ScrollView  indicatorStyle='white'>

                            {this.state.withdrawals?<WithdrawlContainer details={this.state.addr}  withdraw={this.state.withdraw}/>:<DepositContainer details={this.state.addr} deposit={this.state.deposit}/>}
                        </ScrollView>







            </View>

        );

    }
}

const styles =StyleSheet.create({
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5
    },
    buttonText: {
        fontSize: 12,
        fontFamily: 'Ubuntu-R',
        textAlign: 'center',
        color: '#000',
    },
    center:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#B233FF',


    },
    text:{
        fontSize:20,
        color:'#ffffff',
        marginBottom:20,
        height:50,
        backgroundColor: 'transparent',
        fontFamily:'Ubuntu-R',
        textAlign: 'center',
        alignItems:'center',
        justifyContent:'center'

    },
    input:{
        // fontSize:15,
        // color:'#8E30A5'
        // borderRadius:2,
        //
        justifyContent:'center',
        alignSelf:'center',
        // borderColor:'#8E30A5',
        marginTop:180,
        flex:1,
        width:'90%',



    },
    row:{
        flex: 1,
        flexDirection: "row",
        height:40
    },
    button:{

        backgroundColor:'#202020',
        flexDirection:'row',
        height:40,
        borderRadius:35,
        alignItems:'center',
        justifyContent:'center',
        overflow:'hidden',
    },
    btntext:{
        lineHeight:20,
        fontSize:15,
        marginHorizontal:24,
        paddingBottom:10,
        color:'#ffffff',
        fontFamily:'Ubuntu-R',
        textAlign: 'center',
        alignItems:'center',
        justifyContent:'center'

    },
    icons:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        height: 40,
        borderRadius: 5 ,
        margin: 10
    },
    ImageStyle: {
        padding: 10,
        margin: 5,
        height: 35,
        width: 35,
        resizeMode : 'stretch',
        alignItems: 'center'
    }, header:{
        height:55,
        backgroundColor:'#242424',
        justifyContent:'center'
    },
    inner:{
        justifyContent:'center'
    }

});
const mapStateToProps=(state)=>{
    return{
        details:state.dashboard.walletDetails
    }
}

export default connect(mapStateToProps)(WalletContainer);
