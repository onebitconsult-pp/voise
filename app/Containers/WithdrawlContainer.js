import React,{Component} from 'react';
import {Text, View, TextInput, Picker, StyleSheet,FlatList,TouchableWithoutFeedback,ImageBackground,TouchableOpacity} from 'react-native';
import {Icon} from 'react-native-elements';
export default class WithdrawlContainer extends Component {
    constructor(){
        super();
        this.state={
            language:''
        }
    }
    state = {
        names: [
            {
                id: 0,
                name: 'Ben',
            },
            {
                id: 1,
                name: 'Susan',
            },
            {
                id: 2,
                name: 'Robert',
            },
            {
                id: 3,
                name: 'Mary',
            }
        ]
    }

    render(){
        return(

            <View style={{flex:1}}>
                 <View style={{flexDirection:'row',alignItems:'center',marginTop:8,justifyContent:'center'}}>
                     <TextInput underlineColorAndroid='transparent'   style={{borderRadius:6,borderBottomColor:'#fff',color:'#fff',marginHorizontal:8,borderWidth:0.3,width:'90%',marginBottom:8,textAlign: 'center'}} placeholderTextColor='#fff' placeholder='Address'/>


                 </View>
                <View style={{flexDirection:'row',justifyContent:'flex-end',marginRight:'3%'}}>
                    <View style={{justifyContent:'center',backgroundColor:'black',borderBottomColor:'#fff',borderWidth:0.3,height:40,paddingLeft:8,width:'28%',marginRight:'5%',borderRadius:6}}>
                    <Picker
                            selectedValue={this.state.language}
                            onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
                        <Picker.Item label="Eth " value="ethereum" color='white' style={{paddingLeft:6}} />
                        <Picker.Item label="Voise " value="voise" color='white' />
                    </Picker>
                    </View>


                    <TextInput placeholder='Enter the Quantity' placeholderTextColor='white' style={{borderRadius:6,borderBottomColor:'white',borderBottomColor:'#fff',color:'#fff',marginLeft:'2%',width:'48%',marginRight:'3%'}}/>
                </View>


                <View style={{alignSelf:'center',marginVertical:16}}>
                    <TouchableWithoutFeedback >
                        <View style={styles.button}  resizeMode="cover"
                               >

                            <ImageBackground  style={{paddingLeft:30,paddingRight:30,paddingTop:10,paddingBottom:10}} source={require('../images/button.png')}><Text style={styles.btntext}>withdraw now</Text>
                            </ImageBackground>
                        </View>

                    </TouchableWithoutFeedback>
                </View>
                <Text style={styles.text}>Withdrawal History</Text>
                {this.props.withdraw && this.props.withdraw.map((l,i)=>(
                <View style={{backgroundColor:'#000',borderRadius:4,marginBottom:60,marginHorizontal:8,borderWidth:1,borderColor:'#fff'}}>
                    <View style={{marginHorizontal: 8, marginVertical: 8}}>
                        <View style={{flex:1,flexDirection:'row'}}><Text style={{color:'#fff',marginRight:10}}>Date :</Text>
                            <Text style={{color:'#fff',alignSelf:'flex-end'}}>{l.date}</Text>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <Text style={{color:'#fff',marginRight:10}}>Type :</Text>
                            <Text style={{color:'#fff',alignSelf:'center'}}>{l.token}</Text>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <Text style={{color:'#fff',marginRight:10}}>Transcation Id :</Text>
                            <Text style={{color:'#fff',alignSelf:'center'}}>{l.tr_hash}</Text>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <Text style={{color:'#fff',marginRight:10}}>Amount :</Text>
                            <Text style={{color:'#fff',alignSelf:'center'}}>{l.amount}</Text>
                        </View>
                    </View>

                </View>
                    )) }





            </View>
        );
    }
}
const styles =StyleSheet.create({
    center:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#B233FF',


    },
    text:{
        fontSize:20,
        color:'#ffffff',
        alignSelf:'flex-start',
        marginBottom:20,
        marginTop:10,
        // height:50,
        // backgroundColor: 'transparent',
        fontFamily:'Ubuntu-R',
        // textAlign: 'center',
        // alignItems:'center',
        // justifyContent:'center'

    },
    input:{
        // fontSize:15,
        // color:'#8E30A5'
        // borderRadius:2,
        //
        justifyContent:'center',
        alignSelf:'center',
        // borderColor:'#8E30A5',
        marginTop:180,
        flex:1,
        width:'90%',



    },
    row:{
        flex: 1,
        flexDirection: "row",
        height:40
    },
    button:{
        backgroundColor:'#0295f7',
        alignSelf:'center',
        width:'50%',
        justifyContent:'center',
        height:40,
        overflow:'hidden',
        borderRadius:4,

    },
    btntext:{
        fontSize:20,
        color:'#ffffff',
        fontFamily:'Ubuntu-R',
        textAlign: 'center',
        alignItems:'center',
        justifyContent:'center'

    },
    icons:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        height: 40,
        borderRadius: 5 ,
        margin: 10
    },
    ImageStyle: {
        padding: 10,
        margin: 5,
        height: 35,
        width: 35,
        resizeMode : 'stretch',
        alignItems: 'center'
    },

});
