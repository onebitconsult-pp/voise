import React ,{Component} from 'react';
import {ToastAndroid, View,FlatList,Text,ImageBackground,Image,StyleSheet,ScrollView,TouchableWithoutFeedback,Modal,ActivityIndicator} from 'react-native';
import CardComponent from '../Components/CardComponent';
import {userActions} from "../store/actions/dashboard";
import {connect} from 'react-redux';

class genreAlbumContainer extends Component {
    constructor(props){
        super(props);
        this.state={
            genreAlbumLists:null,
            loading:true
        }

    }
    componentWillMount(){
        debugger
        if(this.props.navigation.state.params===undefined){
            this.setState({loading:true});
        }
        this.props.dispatch(userActions.getGenreAlbum(this.props.navigation.state.params.id));

    }
    componentWillReceiveProps(nextProps){
        debugger
        if(nextProps.genrealbums.res){
            this.setState({loading:false});
            this.setState({genreAlbumLists:nextProps.genrealbums.res.genre});
        }
    }

    toAlbum(params){
        debugger
        this.navigation.navigate('Album',{'id':params.id})
    }

    render(){
        return(
            <View style={{flex:1,backgroundColor:'#000'}}>
            { this.state.loading? <View style={{flex:1,justifyContent:'center'}}>
                <ActivityIndicator size="large"/>


            </View>:
                <View style={{flex:1,backgroundColor:'#000',flexDirection:'row'}}>
                {this.state.genreAlbumLists &&

                <FlatList
                    initialNumToRender={10}
                    maxToRenderPerBatch={10}
                    windowSize={10}
                    removeClippedSubviews
                    style={{flexDirection:'column',flex:1}}
                    data={this.state.genreAlbumLists}
                    numColumns={2}
                    keyExtractor={item => item.i_album} //has to be unique
                    renderItem={({ item }) => (<CardComponent genre={true}  key = {item.i_album}  icon={false} id={item.i_album}   onCardPress={this.toAlbum} style={Styles.center} Text={item.album_name} Artist={item.artist_name} imagestyle={Styles.image} Imgurl={item.album_cover} navigation={this.props.navigation}/>
                    )}
                    />


                }

                </View>

             }
            </View>

        )
    }




}
const Styles=StyleSheet.create({
    imgcenter:{
        width:120,
        height:140,
        borderRadius:60



    },
    imgview:{
        flex:1,
        overflow: 'hidden',
        borderRadius:60,
        alignItems:'center',


    },
    Text:{
        marginTop:20,
        padding:17,

    },
    list:{
        flex:0.7,
        backgroundColor:'transparent',


    },
    center: {
        padding: 0, margin: 1,
        height: 170,
        width: 160,
        marginHorizontal:8,
        marginVertical:4,
        marginRight: '5%',
        borderRadius: 4,
        borderColor: '#000',
        backgroundColor:'transparent',

    },
    image: {

        minHeight:140,
        width: 150,
        height: 150,
        marginTop:0,
        paddingTop:-2,
        paddingBottom:-10

    },
    scrollimage:{
        maxHeight:100,
        maxWidth:100,
    },
    button:{
        backgroundColor:'#0295f7',
        alignSelf:'center',
        width:'50%',
        justifyContent:'center',
        height:40,
        overflow:'hidden',
        borderRadius:4,

    },
    btntext:{
        fontSize:20,
        color:'#ffffff',
        fontFamily:'Ubuntu-R',
        textAlign: 'center',
        alignItems:'center',
        justifyContent:'center'

    },
    buttonview:{
        flex:0.1,
        marginTop:20,
        marginBottom:60

    },
    header:{
        height:55,
        backgroundColor:'#242424',
        justifyContent:'center'
    },
    inner:{
        justifyContent:'center'
    }

});

const mapStateToProps=(state)=>{
    return {
        genrealbums:state.dashboard.genrealbums
    }

}
export default connect(mapStateToProps) (genreAlbumContainer);
