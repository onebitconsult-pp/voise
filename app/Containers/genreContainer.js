import React ,{Component} from 'react';
import {
    ToastAndroid,
    View,
    FlatList,
    Text,
    ImageBackground,
    Image,
    StyleSheet,
    ScrollView,
    TouchableWithoutFeedback,
    Modal,
    ActivityIndicator,
    Dimensions
} from 'react-native';
import CardComponent from '../Components/CardComponent';
import {userActions} from "../store/actions/dashboard";
import {connect} from 'react-redux';
const { Width } = Dimensions.get('window').width;
const {fullWidth}=Width/2;
class genreContainer extends Component {
     constructor(props){
         super(props);
         this.state={
             genreLists:null,
             loading:true
         }

     }
    componentWillMount(){
        debugger
        this.props.dispatch(userActions.getGenre());

    }
    componentWillReceiveProps(nextProps){
        debugger
        if(nextProps.genrelist){
            this.setState({loading:false});
            this.setState({genreLists:nextProps.genrelist.res.genre});
        }
    }
     callapi=(gid)=>{
        debugger
         this.props.navigation.navigate('genreAlbum',{'id':gid})


     }

    render(){
        return(
            <View style={{flex:1,backgroundColor:'#000'}}>
                { this.state.loading? <View style={{flex:1,justifyContent:'center'}}>
                        <ActivityIndicator size="large"/>


                    </View>:
            <View style={{flex:1,backgroundColor:'#000',flexDirection:'row'}}>


                <FlatList
                    removeClippedSubviews={false}
                    style={{flexDirection:'column'}}
                    data={this.state.genreLists}
                    numColumns={2}
                    keyExtractor={item => item.g_id} //has to be unique
                    renderItem={({ item }) => (<CardComponent genre={true}  ids={item.g_id} key={item.g_id} onCardPress={(ev)=>{this.callapi(item.g_id)}}
                    Imgurl={item.image_link}
                    imagestyle={Styles.image} style={Styles.center}
                    navigation={this.props.navigation}/>
                    )}
                />


            </View> }
            </View>
        )
    }




}
const Styles=StyleSheet.create({
    imgcenter:{
        width:120,
        height:120,
        borderRadius:60



    },
    imgview:{
        flex:1,
        overflow: 'hidden',
        borderRadius:60,
        alignItems:'center',


    },
    Text:{
        marginTop:20,
        padding:17,

    },
    list:{
        flex:0.7,
        backgroundColor:'transparent',


    },
    center: {
        padding: 0, margin: 0,

        marginTop:0,
        marginRight:0,
        borderRadius: 4,
        borderColor: '#000',
        backgroundColor:'transparent',

    },
    image: {

        minHeight:100,
        height: 100,
        marginTop:0,


    },
    scrollimage:{
        maxHeight:100,
        maxWidth:100,
    },
    button:{
        backgroundColor:'#0295f7',
        alignSelf:'center',
        width:'50%',
        justifyContent:'center',
        height:40,
        overflow:'hidden',
        borderRadius:4,

    },
    btntext:{
        fontSize:20,
        color:'#ffffff',
        fontFamily:'Ubuntu-R',
        textAlign: 'center',
        alignItems:'center',
        justifyContent:'center'

    },
    buttonview:{
        flex:0.1,
        marginTop:20,
        marginBottom:60

    },
    header:{
        height:55,
        backgroundColor:'#242424',
        justifyContent:'center'
    },
    inner:{
        justifyContent:'center'
    }

});

const mapStateToProps=(state)=>{
    return {
        genrelist:state.dashboard.genrelist
    }

}
export default connect(mapStateToProps) (genreContainer);
