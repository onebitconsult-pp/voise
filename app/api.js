import axios from 'axios';
import {AsyncStorage, } from "react-native";
//const BaseURl='https://app.voise.com';
//Api.callApi(api.checklogim, data);
const apuUrls={
    Album:'http://13.58.8.234/voise/api/album.php',
    getBody:'http://13.58.8.234/voise/api/landingpage.php',
    upd:'http://13.58.8.234/voise/api/'

}
const baseUrl='http://52.60.157.75/web/api/';
export const Api={
     getUrl: function (apiCall) {
         return apuUrls[apiCall.name];
     },
     callApi: function (func, data) {
         funnct=JSON.stringify(func);

         let url = this.getUrl(func);
         return AsyncStorage.getItem('credentials').then((details)=>{
            let udetails=JSON.parse(details);

             let req = {
                 url: url,
                 method: 'post',
                 headers: {
                     'UID': udetails.user_id,
                     'TOKEN': udetails.access_token
                 }
             };
             if(data!==undefined||data!==""){
                 req.data=JSON.stringify(data);
             }

             return func(req);

         });
     },
    getBody:function getBody(){

        return axios({method:'post',url:baseUrl+'landingpage.php',headers:global.req.headers}).then((res)=>{

            return res
        })
    },
    loginCheck: function(credentials){
        credentials=JSON.stringify(credentials);

        return axios.post(baseUrl+'signin.php',
            credentials
        )
            .then(function (response) {


                return {
                    response:response,
                    credentials:credentials

                }

            })
            .catch(function (error) {
                console.log(error);
            });
    },
    signUp:function(dataval){
        dataval=JSON.stringify(dataval);
        return axios.post(baseUrl+'signup.php',dataval).then(function(response){
                return {
                    response:response
                }
        }).catch(function (error) {
            if (error.response) {
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(error.request);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log('Error', error.message);
            }
            console.log(error.config);
        });

    },
    Album:function Album (id){


        return axios({method:'post',url:baseUrl+'album.php',data:id,headers:global.req.headers}).then(function(res){
       debugger
            return{
                res:res.data
            }
        })
    },
    ForgotPassword:function(email){

        email={email:email};
        email=JSON.stringify(email);
        return axios.post(baseUrl+'PassRecovery.php',email).then(function (res)
        {
            return{
                res
            }
        })
    },
    slider:function(){

        return axios.get(baseUrl+'fetch_slider_details.php').then(function(res){
        return {

            res:res.data.slider_images
        }
        })
    },
    getProfile:function (id) {
        let i_user={i_user:id};
        return axios({method:'post',url:baseUrl+'profilepage.php',data:i_user,headers:global.req.headers}).then(function (res) {
         debugger
          return{
              res:res.data
          }

      })
    },
    updateProfiles:function (id,formData) {
    return axios({method:'post',url:baseUrl+'update-userprofile.php',data:formData
        ,headers:global.req.headers}).then(function (res) {
         console.log(res);
        return {
            res:res.data
        }

    })
    },
    updateImage:function(data){

         return axios({method:'post',url:baseUrl+'update-userprofilephoto.php',data:data,headers:global.req.headers, validateStatus: (status) =>
        {
            return true;
        }
         }).then(function (res) {
             debugger
             return {
                     res:res.data
             }

         }).catch(error => {
            console.log(error.message);
        })
    },
    search:function (data) {
         return axios({method:'post',url:baseUrl+'search.php',data:data,headers:global.req.headers}).then((res)=>{
             return {
                 res:res.data
             }
         })

    },
    getPlayList:function(){
         return axios({method:'post',url:baseUrl+'list_playlist.php',headers:global.req.headers}).then((res)=>{
            debugger
             return {
                 res:res.data
             }
         })
    },
    getPlayListSongs:function (id) {

         return axios({method:'post',url:baseUrl+'playlist_songs.php',data:id,headers:global.req.headers}).then((res)=>{
             return{
                 res:res.data
             }
         })
    },
    createPlayList:function (name){

         return axios({method:'post',url:baseUrl+'createplaylist.php',data:name,headers:global.req.headers}).then((res)=>{
             debugger
             return {
                 res:res.data
             }

         })
    },
    addToPlayLists:function (id) {
      return axios({method:'post',url:baseUrl+'addtoplaylist.php',data:id,headers:global.req.headers}).then((res)=>{
         debugger
          return {
              res:res.data
          }
      })
    },
    deletePlaylist:function (id) {
     return axios ({method:'post',url:baseUrl+'delete_playlists.php',data:id,headers:global.req.headers}).then((res)=>{
         debugger
         return {
             res:res.data
         }
     })
    },
    deleteSong:function(id){
         return axios ({method:'post',url:baseUrl+'delete_songs_playlist.php',data:id,headers:global.req.headers}).then((res)=>{
             return {
                 res:res.data
             }
         })
    },
    changeuserPass:function(data){
         return axios ({method:'post',url:baseUrl+'updatechangepassword.php',data:data,headers:global.req.headers}).then((res)=>{
             debugger
             return {
                 res:res.data
             }
         })
    },
    getWallet:function(){
         return axios({method:'post',url:baseUrl+'mywallet_details.php',headers:global.req.headers}).then((res)=>{
             debugger
             return {
                 res:res.data
             }
         })
    },
    getGenre:function(){
         debugger
         return axios({method:'post',url:baseUrl+'show_genre.php',headers:global.req.headers}).then((res)=>{
             debugger
             return{
                 res:res.data
             }
         })
    },
    getAlbumsGenre:function (id) {
        let g_id={g_id:id}
          g_id=JSON.stringify(g_id);
        return axios({method:'post',url:baseUrl+'genre.php',data:g_id,headers:global.req.headers}).then((res)=>{
            debugger
            return{
                res:res.data
            }
        })
    }


}
