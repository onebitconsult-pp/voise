import React,{Component} from 'react';
import {View, Text, TextInput, ImageBackground, StyleSheet,Image,TouchableWithoutFeedback,Alert} from 'react-native';
import {userActions} from "../store/actions/dashboard";
import {connect} from 'react-redux';
import dashboard from "../store/reducers/dashboard";

 class ForgotPassword extends Component{
  constructor(props){
      super(props);
      this.state={
          email:'',
          fill:'',
          alert:false,
          sending:false
      }
      this.getpassword=this.getpassword.bind(this);

  }
  getpassword(){
      if(this.state.email!=''){
          this.setState({sending:true});
          this.props.dispatch(userActions.forgotPass(this.state.email));
      }else{
          this.setState({fill:'please type email'});
      }


  }
  componentWillReceiveProps(nextProps){
      this.setState({sending:false});

     this.setState({alert:nextProps.MESSAGE});


  }


render(){
      if(this.state.sending==true){
          return (

          <View style={{flex:1,backgroundColor:'#000',justifyContent:'center',alignItems:'center'}}>

                  <Text style={{color:'#fff'}} >Sending req...
                  </Text>


          </View>
              )
          }
      else{
     return(
 <View style={styles.center} >


 <ImageBackground
 style={{
     backgroundColor: '#ccc',
     flex: 1,
     width: '100%',
     position:'absolute',
     height: '100%',
     justifyContent: 'center',

 }}
 source={require('./splash.png')}
 >



 <View style={styles.input}>
     {this.state.alert &&<Text style={{color:'red',fontFamily:'Ubuntu-R'}}>{this.state.alert}</Text>}

 <View style={styles.icons}>
 <Image source={require('../logos/ic-mail.png')} style={styles.ImageStyle} />
 <TextInput
 value={this.state.email}
 style={{flex:1,color:'white'}}
 label={'Email'}
 placeholder={'Email'}
 placeholderTextColor="white"
 onChangeText={(text)=>this.setState({email:text,fill:false})}
 />
 </View>
 <TouchableWithoutFeedback onPress={()=>this.getpassword()}>
 <View style={styles.button}  resizeMode="cover" borderRadius={35} >
 <ImageBackground
 style={{paddingLeft:30,paddingRight:30,paddingTop:10,paddingBottom:10}}
 source={require('../images/button.png')}>
 <Text style={styles.btntext}>Submit</Text>
 </ImageBackground>
 </View>
 </TouchableWithoutFeedback>
 </View>


     <TouchableWithoutFeedback onPress={()=>this.props.navigation.navigate('Login')}>
         <View style={{alignSelf:'center',marginTop:'4%'}}>
             <Text style={{color:'#0295f7',paddingLeft:'2%',fontSize:16}}>LogIn</Text>
         </View>
     </TouchableWithoutFeedback>

 </ImageBackground>
 </View>
 );

      }

}
}
const styles =StyleSheet.create({
    center:{
        flex:1,

        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#B233FF',


    },
    text:{
        fontSize:20,
        color:'#ffffff',
        marginBottom:20,
        height:50,
        backgroundColor: 'transparent',
        fontFamily:'Ubuntu-R',
        textAlign: 'center',
        alignItems:'center',
        justifyContent:'center'

    },
    input:{
        // fontSize:15,
        // color:'#8E30A5'
        // borderRadius:2,
        //
        justifyContent:'center',
        alignSelf:'center',
        // borderColor:'#8E30A5',
        marginTop:'35%',
        // flex:1,
        width:'90%',



    },
    button:{
        backgroundColor:'#0295f7',
        alignSelf:'center',
        width:'50%',
        justifyContent:'center',
        height:40,
        overflow:'hidden',
        borderRadius:4,

    },
    overlays:{
        position:'absolute',
        height:60,
        width:60,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#000',


    },
    btntext:{
        fontSize:20,
        color:'#ffffff',
        fontFamily:'Ubuntu-R',
        textAlign: 'center',
        alignItems:'center',
        justifyContent:'center'

    },
    icons:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        height: 50,
        borderRadius: 5 ,
        marginTop:'10%',
        marginBottom:20
    },
    ImageStyle: {
        padding: 10,
        margin: 5,
        height: 20,
        width: 20,
        resizeMode : 'stretch',
        alignItems: 'center'
    },

});
  const mapStateToProps=(state)=>{
      return {

          FORGOT:state.dashboard.FORGOT,
          MESSAGE:state.dashboard.MESSAGE,

      }

}
 export default connect(mapStateToProps)(ForgotPassword);