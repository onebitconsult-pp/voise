import React, { Component } from 'react';
import { View , Text , StyleSheet,Image,ImageBackground,TouchableWithoutFeedback,AsyncStorage,ActivityIndicator } from 'react-native';
import {TextInput} from 'react-native';
import SignUp from './SignUp';
import { StatusBar } from 'react-native';
import Dashboard from "./screens/DashBoard";
import Login from './Login';
import {Api} from '../api';
import {userActions } from "../store/actions/dashboard";
import { connect } from 'react-redux';
import {login} from '../store/actions/dashboard';
class Home extends Component{

    constructor(props){
        super(props);

        this.login = this.login.bind(this);


    }
    componentDidMount() {


    }
    componentWillReceiveProps(nextProps){

        this.props.isLoggedIn==nextProps.isLoggedIn;

    }


    login(credentials){
        var data={};
        data= {
            email: credentials.email,
            password: credentials.password
        }
        const { dispatch } = this.props;
        dispatch(userActions.appLogin(data));
        //this.//setState({isLoggedIn : login.loggedIn});
        };



    render(){
        return(
            this.props.isLoading?<View style={{flex:1,justifyContent:'center',backgroundColor:'black'}}>
                    <ImageBackground style={{
                    backgroundColor: '#000',
                    flex: 1,
                    width: '100%',
                    position:'absolute',
                    height: '100%',
                    justifyContent: 'center',
                }}source={require('./splash.png')}>
                    <ActivityIndicator size="large" color="#0000ff" />
                    </ImageBackground>
                </View>:
            this.props.isLoggedIn ?
                <View style={{flex:1,justifyContent:'center',backgroundColor:'black'}}>
            <Dashboard navigation={this.props.navigation}/>
                </View>
           :
            <Login onLogin={this.login} error={this.props.error} navigation={this.props.navigation} message={'invalid username password'} loginStatus={this.props.isLoggedIn}/>
        );

    }

}
const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.dashboard.isLoggedIn,
        error:state.dashboard.error,
        isLoading:state.dashboard.isLoading,



    };
}
export default connect(mapStateToProps)(Home);