import React, { Component } from 'react';
import {
    View, Text, StyleSheet, Image, ImageBackground, TouchableWithoutFeedback, ActivityIndicator,
    AsyncStorage,AppState
} from 'react-native';
import {TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import SignUp from './SignUp';
import { StatusBar } from 'react-native';
import {Api} from '../api';
import {connect} from 'react-redux';
import {userActions} from "../store/actions/dashboard";



class Login extends Component{

    constructor(props){
        super(props);
        this.state={
            email:'',
            password:'',
            eye:true,
            loader:true,

        };

        this.login=this.login.bind(this);
    }



        login(){

            var data={};
            data= {
                email: this.state.email,
                password: this.state.password
            }
            const { dispatch } = this.props;
            dispatch(userActions.appLogin(data));
            //this.//setState({isLoggedIn : login.loggedIn});
        };
        //this.props.onLogin(this.state);

    componentWillMount(){
        debugger

        if(this.props.gif==false) {
            this.setState({gif: true});
        }else{
            this.setState({gif:false});
        }
        AppState.addEventListener('change', (state) => {
            if (state === 'active') {
                console.log('state active');
            }
            if(state === 'background'){
                console.log('background');
            }
        });
        if(this.props.isLoggedIn===true&&this.props.isLoggedIn!==undefined){
            this.props.navigation.navigate('DrawerStack');
        }


        // if(this.props.SIGNOUT==true){
        //     this.props.dispatch(userActions.signIn());
        //
        // }
    }
    componentDidMount(){
        if(this.props.gif==false) {
            setTimeout(() => {
                this.props.dispatch(userActions.logingif());
                this.setState({gif: false,loader:true});
                AsyncStorage.getItem("credentials").then((credentials) => {
                    if (credentials != null) {

                        let udetails = JSON.parse(credentials);

                        global.req = {
                            method: 'post',
                            headers: {
                                'UID': udetails.user_id,
                                'TOKEN': udetails.access_token,
                            }
                        };
                        global.userDetails = {
                            wallet_addr: udetails.wallet_addr,
                        };

                        this.props.dispatch(userActions.loginsuccess());

                        // this.props.navigation.navigate('DrawerStack');

                    } else {
                        this.setState({loader: false});
                    }

                })
            }, 1000);
        }else{
            // this.props.dispatch(userActions.logingif());
            this.setState({gif: false,loader:true});
            AsyncStorage.getItem("credentials").then((credentials) => {
                if (credentials != null) {

                    let udetails = JSON.parse(credentials);

                    global.req = {
                        method: 'post',
                        headers: {
                            'UID': udetails.user_id,
                            'TOKEN': udetails.access_token,
                        }
                    };
                    global.userDetails = {
                        wallet_addr: udetails.wallet_addr,
                    };

                    this.props.dispatch(userActions.loginsuccess());

                    // this.props.navigation.navigate('DrawerStack');

                } else {
                    this.setState({loader: false});
                }

            })

        }


    }
    componentWillReceiveProps(nextprops){
    debugger
        if(nextprops.gif===true){
        this.setState({gif:false});
        }
        if(nextprops.isLoggedIn===true){

            debugger
            AsyncStorage.getItem('credentials').then((details)=> {
                let udetails = JSON.parse(details);

                global.req= {
                    method: 'post',
                    headers: {
                        'UID': udetails.user_id,
                        'TOKEN': udetails.access_token
                    }
                };
                this.props.navigation.navigate('DrawerStack');

            });

        }else{

            this.setState({loader:false});
        }


            this.setState({error:nextprops.error});

    }


    render(){
        return(



            <View style={styles.center}>
                <StatusBar
                    backgroundColor="black"
                    barStyle="light-content"
                />

                {this.state.gif?
                    <View style={{flex:1,justifyContent:'center',alignItems:'center',width:'100%',height:'100%',backgroundColor:'black'}}>
                        <Image source={require('../images/splash.png')} style={{width:'100%',height:'100%'}} />
                    </View>:
                <ImageBackground
                    style={{
                        backgroundColor: '#ffffff',
                        flex: 1,
                        width: '100%',
                        position:'absolute',
                        height: '100%',
                        justifyContent: 'center',
                    }}

                        source={require('./splash.png')}

                >
                    {this.state.loader?
                        <ActivityIndicator size="large" color="#0000ff" />:
                    <View style={styles.input}>


                        {this.state.error && <Text style={{color:'red'}}>{this.state.error}</Text>}

                        <View style={styles.icons}><Image source={require('../logos/ic-user.png')} style={styles.ImageStyle} />
                            <TextInput  style={{flex:1,color:'white'}} label={'Email'} placeholder={'Email'} placeholderTextColor="white" value={this.state.email} onChangeText={(text) => this.setState({email: text})}
                            />
                        </View>
                        <View style={styles.icons}><Image source={require('../logos/icpass.png')} style={styles.ImageStyle} />
                            <TextInput secureTextEntry={this.state.eye?true:false} style={{flex:1,color:'white'}} label={'Password'} placeholder={'Password'} placeholderTextColor="white" value={this.state.password} onChangeText={(text) => this.setState({password: text,error:false})} />
                            {this.state.eye?<Icon name="eye"  color="white" size={16} onPress={()=>this.setState({eye:false})}/>:<Icon name="eye-slash" size={16} color="white" onPress={()=>this.setState({eye:true})}/>}
                        </View>

                        <TouchableWithoutFeedback  onPress={()=>this.login()} >
                            <View style={styles.button}  resizeMode="cover"
                                  borderRadius={35} >

                                <ImageBackground  style={{paddingLeft:30,paddingRight:30,paddingTop:10,paddingBottom:10}} source={require('../images/button.png')}><Text style={styles.btntext}>Log In</Text>
                                </ImageBackground>
                            </View>

                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={()=>this.props.navigation.navigate('SignUp')}>
                            <View style={{alignSelf:'center',marginTop:'4%'}}>
                                <Text style={{fontFamily:'Ubuntu-R',color:'white',fontSize:14}}>New User?</Text>
                                <Text style={{color:'#0295f7',paddingLeft:'2%'}}>Sign-Up</Text>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={()=>this.props.navigation.navigate('Forgot')}>
                            <View style={{alignSelf:'center',marginTop:'4%'}}>

                                <Text style={{color:'#0295f7',paddingLeft:'2%'}}>Forgot Password?</Text>
                            </View>
                        </TouchableWithoutFeedback>





                    </View>
                    }


                </ImageBackground> }


            </View>
            // <screens/>
        );

    }

}
const styles =StyleSheet.create({
    center:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#ffffff',


    },
    text:{
        fontSize:20,
        color:'#ffffff',
        marginBottom:20,
        height:50,
        backgroundColor: 'transparent',
        fontFamily:'Ubuntu-R',
        textAlign: 'center',
        alignItems:'center',
        justifyContent:'center'

    },
    input:{
        // fontSize:15,
        // color:'#8E30A5'
        // borderRadius:2,
        //
        justifyContent:'center',
        alignSelf:'center',
        // borderColor:'#8E30A5',
        marginTop:180,
        flex:1,
        width:'90%',



    },
    button:{
        backgroundColor:'#0295f7',
        alignSelf:'center',
        width:'50%',
        justifyContent:'center',
        height:40,
        overflow:'hidden',
        borderRadius:4,


    },
    btntext:{
        fontSize:20,
        color:'#ffffff',
        fontFamily:'Ubuntu-R',
        textAlign: 'center',
        alignItems:'center',
        justifyContent:'center'

    },
    icons:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        height: 40,
        borderRadius: 5 ,
        margin: 10
    },
    ImageStyle: {
        padding: 10,
        margin: 5,
        height: 20,
        width: 20,
        resizeMode : 'stretch',
        alignItems: 'center'
    },
    row:{
        flex: 1,
        flexDirection: "row"
    }


});
const mapStateToProps=(state)=>{
    return{
        SIGNOUT:state.dashboard.SIGNOUT,
        isLoggedIn:state.dashboard.isLoggedIn,
        error:state.dashboard.error,
        gif:state.dashboard.gif
    }
}
export default connect(mapStateToProps)(Login);
