import React,{Component} from 'React';
import Signup from './SignUp';
import {View} from 'react-native';
import ForgotPassword from './ForgotPassword';
import Login from './Login';
import Home from './Home';
import {StackNavigator,DrawerNavigator,TabNavigator,TabRouter,addNavigationHelpers,TabBarBottom} from 'react-navigation';
import Dashboard  from './screens/DashBoard';
import genreAlbumContainer from '../Containers/genreAlbumContainer';
import genreContainer from '../Containers/genreContainer';
import Myplaylist from '../Containers/Myplaylist';
import Radio from '../Containers/Radio';
import Podcast from '../Containers/Podcast';
import AlbumCover from '../Containers/AlbumCover';
import WalletContainer from '../Containers/WalletContainer';
import Profile from '../Containers/ProfileSettings';
import Playerview from '../Containers/Playerview';
import PlayerContainer from '../Containers/PlayerContainer';
import Search from '../Containers/Search';
import Logout from './logout';
import Contact from '../Containers/Contact';
import PlaylistSongs from '../Components/PlaylistSongs';
import {Header} from 'react-native-elements';
import Icon from 'react-native-vector-icons/Feather';
import HeaderComponent from '../Components/HeaderComponent';
import SearchComponent from '../Components/SearchComponent';
import FooterComponent from '../Components/FooterComponent';


// const TabStack=TabNavigator({
//     Wallet:{
//         screen:WalletContainer,
//         navigationOptions: {
//             tabBarVisible: true,
//         },
//
//     },
// },{
//     navigationOptions: ({navigation})=>({
//         headerLeft:<Icon name={'back'} color={'#fff'} onPress={()=>navigation.navigate('Home')}/>,
//     }),
//     headerMode:'float'
// });
// const TabNavigation = StackNavigator({
//     TabStack: { screen: TabStack,
//     },
// },{
//     navigationOptions: ({navigation})=>({
//         headerLeft:<Icon name={'back'} onPress={navigation.navigate('Home')}/>,
//     }),
//     headerMode:'float'
// });
// drawer stack
const Player=TabNavigator({
    screen:FooterComponent
},{
    tabBarComponent: ({navigation}) => <FooterComponent navigation={navigation}/>,
    tabBarPosition: 'bottom',
    animationEnabled: true,
    swipeEnabled: true,
    tabBarVisible: true,

})
const DrawerStac =DrawerNavigator({
    Home: {
        screen:TabNavigator({
            Home:{
        screen: TabNavigator({
            Home: {
                screen: StackNavigator({
                    Home: {
                        screen: Home,
                        navigationOptions: {
                            header: () => null
                        },
                    },
                    Album: {
                        screen: AlbumCover,
                        navigationOptions: {
                            header: () => null
                        }
                    },

                    Player:{
                        screen:Playerview,
                        navigationOptions:{
                            header:()=>null
                        }
                    },


                }),
            },
            genre: {screen: StackNavigator({
                    genre: {
                        screen: genreContainer,
                        navigationOptions: {
                            header: () => null

                        },
                    },
                    genreAlbum: {
                        screen:genreAlbumContainer,
                        navigationOptions: {
                            header: () => null

                        },
                    },
                    Album: {
                        screen: AlbumCover,
                        navigationOptions: {
                            header: () => null
                        },
                    },
                },{initialRoute: 'genre'}),
                },
            MyPlaylist: {screen: StackNavigator({
                    MyPlaylist:{
                        screen:Myplaylist,
                        navigationOptions:{
                            header:()=>null

                        }
                    }, PlaylistSongs:{
                        screen:PlaylistSongs,
                        navigationOptions:{
                            header:()=>null,


                        }
                    },
                },{initialRoute: 'MyPlaylist'})
                },
            Wallet: {
                screen: WalletContainer, navigationOptions: {
                    header: () => null
                }
            },
        }, {
            tabBarComponent: ({navigation}) => <FooterComponent navigation={navigation}/>,
            tabBarPosition: 'top',
            animationEnabled: false,
            swipeEnabled: true,
            tabBarVisible: true,
            lazyLoad: true

        }),},Search: {
                screen: Search,
                navigationOptions: {
                    header: () => null,

                },

            },
            },{
            tabBarComponent: ({navigation}) => <PlayerContainer navigation={navigation}/>,
            tabBarPosition: 'bottom',
            animationEnabled: false,
            swipeEnabled: false,
            tabBarVisible: true,
            lazyLoad: true
        }),
        navigationOptions: {
            drawerIcon: <Icon name="home" color={'white'} size={24} style={{color:'white'}}/>,



        },
    },
    'My Profile':{
        screen:Profile,
        navigationOptions: {
            header: () => null,
            drawerIcon: <Icon name="user" color={'white'} size={24} style={{color:'white'}}/>,
        },
    },
    'Contact Us':{
        screen:Contact,
        navigationOptions: {
            header: () => null,
            drawerIcon: <Icon name="phone-call" color={'white'} size={24} style={{color:'white'}}/>,
        },
    },
    Logout:{
        screen:Logout,
        navigationOptions:{
            header:()=>null,
            drawerIcon: <Icon name="log-out" color={'white'} size={24} style={{color:'white'}}/>,
        }
    }
}, {

    activeBackgroundColor:'#049ff2',
    activeTintColor:'#049ff2',
    drawerBackgroundColor: "black",
    contentOptions: {
        labelStyle: {
            fontFamily: 'Ubuntu-R',
            color: 'white',
        },
        activeTintColor:'#049ff2',

    }
});
const DrawerNavigation = StackNavigator({
    DrawerStack: {
        screen: DrawerStac,

    },

},{
    navigationOptions:({navigation})=>({
       header:<HeaderComponent navigation={navigation}/>
    }),
    headerMode:'float',
    gesturesEnabled:true
});

// login stack
const LoginStack = StackNavigator({

    Login: {
        screen: Login,
        navigationOptions: {
            header: () => null,
        }
    },
    SignUp: {
        screen: Signup,
        navigationOptions: {
            header: () => null,
        }
    },
    Forgot: {
        screen: ForgotPassword,
        navigationOptions: {
            header: () => null,
        }
    },

    Profile:{
        screen:Profile,
        navigationOptions:{
            header : () =>null,
        }
    },


},{

    headerMode:'float'
});

// Manifest of possible screens
const PrimaryNav = StackNavigator({
    loginStack: { screen: LoginStack,
        navigationOptions: {
           header:()=>null
        }
       ,
    },
    drawerStack: { screen: DrawerNavigation,
        navigationOptions: {
         header:()=>null
        },
    },




}, {
    // Default config for all screens


    initialRouteName: 'loginStack'
})

export default PrimaryNav