import React, { Component } from 'react';
import { View , Text , StyleSheet,Image,ImageBackground,TouchableWithoutFeedback,TextInput,Alert } from 'react-native';
// import App from './index';
import {Overlay} from '@shoutem/ui';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Api} from '../api';
import {StackNavigator} from 'react-navigation';
export default class SignUp extends Component{
    constructor(props){
        super(props);
        this.state={
            email:'',
            password:'',
            firstname:'',
            lastname:'',
            confirmpassword:'',
            eye:true,
            eye1:true
        };
        this.signup=this.signup.bind(this);
    }
    signup(){
        this.state.error=false;
        this.state.failed=false;
      if(this.state.password!==this.state.confirmpassword){
          this.setState({error:'Password Mismatch'})
      }
      else{

            this.setState({Pending:true});

          var data={};
          data={
              email:this.state.email,
              password:this.state.password,
              firstname:this.state.firstname,
              lastname:this.state.lastname

          }
          Api.signUp(data).then(
              (res)=>{
                  this.state.Pending=false;
                  if(res.response.data.api_status==200){
                      this.setState({failed:res.response.data.message})
                      Alert.alert(

                          // This is Alert Dialog Title
                          res.response.data.message,

                          // This is Alert Dialog Message.
                          'Go To Login',
                          [
                              // First Text Button in Alert Dialog.
                              {text: 'login', onPress: () => this.props.navigation.navigate('Login')},



                          ]

                      )

                  }
                  else{
                      this.setState({failed:res.response.data.message})

                  }


              }
          )
      }
    }

    render(){
        return(
            <View style={styles.center}>


                <ImageBackground
                    style={{
                        backgroundColor: '#ccc',
                        flex: 1,
                        width: '100%',
                        position:'absolute',
                        height: '100%',
                        justifyContent: 'center',
                    }}
                    source={require('./splash.png')}
                >


                    <View style={styles.input}>
                        {this.state.Pending && <Overlay style={{backgroundColor:'#fff', height:60}}>
                            <Text>Please Wait while we create your account</Text>
                        </Overlay>}

                        <Text style={{color:'white',fontSize:24,fontFamily:'Ubuntu-R',alignSelf:'center',marginTop:15,marginBottom:5}}>Sign-Up </Text>
                        {this.state.failed  && <Text style={{color:'red',fontFamily:'Ubuntu-R'}}>{this.state.failed}</Text>}
                        {this.state.error  && <Text style={{color:'red',fontFamily:'Ubuntu-R'}}>{this.state.error}</Text>}
                        <View style={styles.icons}>

                            <TextInput style={{flex:1,color:'white'}} label={'Email'} value={this.state.email} placeholder={'Email'} placeholderTextColor="white" onChangeText={(text) => this.setState({email: text,failed:false})}
                            />
                        </View>
                        <View style={styles.icons}>

                                <TextInput style={{flex:1,color:'white'}} label={'FirstName'} value={this.state.firstname} placeholder={'First Name'} placeholderTextColor="white" onChangeText={(text) => this.setState({firstname: text})}
                                />


                                <TextInput style={{flex:1,color:'white'}} label={'LastName'} placeholder={'Last Name'} value={this.state.lastname} placeholderTextColor="white" onChangeText={(text) => this.setState({lastname: text})}
                                />


                        </View>

                        <View style={styles.icons}>
                            <TextInput style={{flex:1,color:'white'}} secureTextEntry={this.state.eye?true:false} label={'Password'} placeholder={'Password'} value={this.state.password} placeholderTextColor="white" onChangeText={(text) => this.setState({password: text,error:false})} />
                            {this.state.eye?<Icon name="eye"  color="white" size={16} onPress={()=>this.setState({eye:false})}/>:<Icon name="eye-slash" size={16} color="white" onPress={()=>this.setState({eye:true})}/>}
                            <TextInput secureTextEntry={this.state.eye1?true:false} style={{flex:1,color:'white'}}  label={'ConfirmPass'} placeholder={'Confirm Password'} value={this.state.confirmpassword} placeholderTextColor="white" onChangeText={(text) => this.setState({confirmpassword: text,error:false})}/>
                            {this.state.eye1?<Icon name="eye"  color="white" size={16} onPress={()=>this.setState({eye1:false})}/>:<Icon name="eye-slash" size={16} color="white" onPress={()=>this.setState({eye1:true})}/>}
                        </View>
                        <View style={styles.icons}>

                        </View>

                        <TouchableWithoutFeedback >
                            <View style={styles.button}  resizeMode="cover"
                                  borderRadius={35} >

                                <ImageBackground  style={{paddingLeft:30,paddingRight:30,paddingTop:10,paddingBottom:10}} source={require('../images/button.png')}><Text style={styles.btntext} onPress={()=>this.signup()}>SignUp</Text>
                                </ImageBackground>
                            </View>

                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={()=>this.props.navigation.navigate('Login')}>
                            <View style={{alignSelf:'center',marginTop:'1%'}}>
                                <Text style={{color:'#0295f7',paddingLeft:'2%',fontSize:16}}>LogIn</Text>
                            </View>



                        </TouchableWithoutFeedback>

                    </View>

                </ImageBackground>

            </View>
        );
    }
}
const styles =StyleSheet.create({
    center:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#B233FF',


    },
    text:{
        fontSize:20,
        color:'#ffffff',
        marginBottom:20,
        height:50,
        backgroundColor: 'transparent',
        fontFamily:'Ubuntu-R',
        textAlign: 'center',
        alignItems:'center',
        justifyContent:'center'

    },
    input:{
        // fontSize:15,
        // color:'#8E30A5'
        // borderRadius:2,
        //
        justifyContent:'center',
        alignSelf:'center',
        // borderColor:'#8E30A5',
        marginTop:180,
        flex:1,
        width:'95%',



    },
   row:{
    flex: 1,
    flexDirection: "row",
       height:40
 },
    button:{
        backgroundColor:'#0295f7',
        alignSelf:'center',
        width:'50%',
        justifyContent:'center',
        height:40,
        overflow:'hidden',
        borderRadius:4,

    },
    btntext:{
        fontSize:20,
        color:'#ffffff',
        fontFamily:'Ubuntu-R',
        textAlign: 'center',
        alignItems:'center',
        justifyContent:'center'

    },
    icons:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        height: 40,
        borderRadius: 5 ,
        margin: 10
    },
    ImageStyle: {
        padding: 10,
        margin: 5,
        height: 35,
        width: 35,
        resizeMode : 'stretch',
        alignItems: 'center'
    },

});