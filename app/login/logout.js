import {View,ActivityIndicator} from 'react-native';
import React ,{Component} from 'react';
import {AsyncStorage} from 'react-native';
import {userActions} from "../store/actions/dashboard";
import {connect} from 'react-redux';
import {NavigationActions} from "react-navigation";


 class Logout extends Component {

     componentWillMount() {



         AsyncStorage.clear();
         this.props.dispatch(userActions.signOut());

     }
     componentWillReceiveProps(nextProps){
              if(nextProps.isPlaying==true){
                  global.sounds.stop();
                  delete global.sounds;
              }
         if(nextProps.isLoggedIn==false){
             this.props.navigation.dispatch(NavigationActions.reset({
                 index: 0, key: null, actions: [NavigationActions.navigate({ routeName: 'loginStack' })]
             }));
         }
     }
     render(){
         return (
             <View style={{flex:1}}>
                 <ActivityIndicator size={'small'}/>

             </View>
         );
     }



 }

 const mapStateToProps=(state)=>{
     return {
     isLoggedIn:state.dashboard.isLoggedIn,
         isPlaying: state.player.isPlaying,
     }
 }
export default connect(mapStateToProps)(Logout);
