import {LOGIN,LOGIN_SUC,GENREALBUMS,GENRELIST,GIF,LOGIN_INPROGRESS,ERROR,CURRENT_ALBUM,FORGOT,SIGNOUT,SLIDER,SEARCH,USERDETAILS,UPDATEPROFILE,BODY,UPLOAD,PLAYLIST,PLAYLISTSONGS,PLAYLISTMESSAGE,PLAYLISTSELECTEDSONG,USERPASSWORD,WALLET,SEARCHEMPTY} from "../../constants";
import {Api} from '../../../api';
import {AsyncStorage, ToastAndroid} from 'react-native';
export const  userActions = {
    logingif,
   appLogin,
    signIn,
    loginsuccess,
    logininprog,
    forgotPass,
    albums,
    signOut,
    slider,
    search,
    cancelSearch,
    getdetails,
    updateProfile,
    updateProfileImage,
    getBody,
    searchdata,
    getPlayLists,
    playListSongs,
    addPlayList,
    addToPlayList,
    selctedSong,
    deletePlaylist,
    deletesongPlaylist,
    changePass,
    getWallet,
    emptySearch,
    getGenre,
    getGenreAlbum,
    setSearch
}
  function setSearch() {
      return { type:SEARCHEMPTY,searchbar:true}
  }
 function logingif(){
    return { type:GIF,gif:true}
 }
  function emptySearch(){
    return {
        type:SEARCHEMPTY,text:false,searchbar:false
    }
  }
 function getWallet(){
    return dispatch=>{
        Api.getWallet().then((res)=>{
          dispatch(walletDetails(res.res));
        })
    }
 }
 function walletDetails(res){
    return {type:WALLET , details:res}
 }
function changePass(data) {
   return dispatch=>{
       Api.changeuserPass(data).then((res)=>{
           if(res.res.api_status==200){
               dispatch(changepassword(res.res.message));
           }
       })
   }
}
function changepassword(message){
    return {type:USERPASSWORD,message:message}
}
function searchdata(dat){
   return dispatch=>{
       let data={
           items:dat
       }
       data=JSON.stringify(data);
       Api.search(data).then((res)=>{

          dispatch(searchresult(res));
       })
   }
}
function searchresult(res){
    return {type:SEARCH,data:res}
}
  function updateProfileImage(data){
      return dispatch=> {
          Api.updateImage(data).then(function(res){
              debugger
            dispatch(updateImage(res.res.data[0]));
          })
      }
  }
 function updateImage(res){
       return {type:UPLOAD,url:res}
}
   function getBody() {

       return dispatch=>{
           Api.getBody().then(function(res){

               dispatch(bodyDetails(res.data));
           })
       }

   }
   function bodyDetails(details){

       return {type:BODY,bodydetails:details}
   }

   function cancelSearch() {
       return { type:SEARCH,search:false}

   }
   function search(){
       return { type:SEARCH,search:true}
   }
   function slider() {
      return dispatch=>{
          Api.slider().then((res)=>{

              let load=res.res.map(function (item) {
               return item=0;
              })

              dispatch(showSliderImages(res.res,load));
          })
      }
   }
   function showSliderImages(res,load){
       return {
           type:SLIDER,imgList:res,load:load
       }
}



   function signOut () {
    return {type:SIGNOUT ,SIGNOUT:true,isLoggedIn:false}
   }

   function  appLogin(credentials)  {

       return dispatch => {
           dispatch(error(undefined));
           Api.loginCheck(credentials).then(
               (res) => {

                   if (res.response.data.api_status == 200) {

                       let cre ={
                           email:res.response.data.data[0].email,
                           user_id:res.response.data.data[0].id,
                           access_token:res.response.data.data[0].a_tok,
                           wallet_addr: res.response.data.data[0].eth_address,
                       };
                     AsyncStorage.setItem('credentials',JSON.stringify(cre)).then((details)=>{
                         dispatch(loginsuccess())
                         });

                   } else {
                       dispatch(error(res.response.data.message))

                   }

               }
           )
       }



            // type: LOGIN,
            // isLoggedIn: true,

        }
        function albums(id) {
    debugger
       return dispatch => {
           let ids={
               i_album:id
           };
           ids=JSON.stringify(ids);
           Api.Album(ids).then(function (res) {

              let albumdetails=res;
              debugger
             dispatch(albumdata(albumdetails));

           })
       }


        }
        function signIn(){
       return {
           type:SIGNOUT,SIGNOUT:false
       }

        }

        function forgotPass(email) {
         return dispatch=>{
             Api.ForgotPassword(email).then(function (res){

                if(res.res.data.api_status===200){
                    dispatch(forgot(res.res.data.message));
                }else{
                    dispatch(forgot(res.res.data.message))
                }

             })
         }

        }
        function forgot(message) {
           return { type:FORGOT, FORGOT:true,MESSAGE:message}
        }

        function loginsuccess() {
              debugger
            return {type: LOGIN, isLoggedIn: true}

        }

        function error(message) {

            return {type: ERROR, message};
        }
        function albumdata(albumdetails) {

            return{ type:CURRENT_ALBUM, albumdetails:albumdetails
            }
            }
        function logininprog() {
            return {type:LOGIN_INPROGRESS ,isLoading:false}

        }
        function getdetails(){
        return dispatch => {

        let userid=AsyncStorage.getItem('credentials').then((details)=>{
            udetails=JSON.parse(details);
            Api.getProfile(udetails.user_id).then((res)=>{
                dispatch(fetchdetails(res.res));
            })

        })

            }
        }
        function fetchdetails(details){
         return {type:USERDETAILS,details:details,status:400}
        }
        function updateProfile(formData) {
        return dispatch =>{



                Api.updateProfiles(udetails.user_id,formData).then((res)=>{
                    debugger
                    dispatch(updateprofile(res.res.api_status));
                    }
                )



        }

        }
        function updateprofile(data){
       return {
           type:UPDATEPROFILE,status:data
       }
}
    function getPlayLists() {
     return dispatch =>{
         Api.getPlayList().then((res)=>{
            dispatch(playlists(res.res));
         })
     }
    }
    function playlists(res){
    return {
        type:PLAYLIST,lists:res,created:false,message:undefined
    }
    }
    function playListSongs(id){
      return dispatch=>{
          ids={i_playlist:id}
          ids=JSON.stringify(ids);
          Api.getPlayListSongs(ids).then((res)=>{
              debugger
             dispatch(playlistSong(res.res));
          })
      }
    }
    function playlistSong(list) {
        return {type:PLAYLISTSONGS,songsdata:list}
    }
    function addPlayList (name) {
      return dispatch =>{
          names={playlistname:name}
          names=JSON.stringify(names);
          Api.createPlayList(names).then((res)=>{
            if(res.res.api_status==200){
                dispatch(playlistCreated());
            }else{
                dispatch(createdError());
            }

          })
      }

    }
    function createdError() {
      return {type:PLAYLIST,error:true}
    }
    function playlistCreated(){
      return { type:PLAYLIST, created:true}
    }
    function addToPlayList(id,song_id) {
        return dispatch =>{
          ids={i_playlist:id,i_song:song_id}
            Api.addToPlayLists(ids).then((res)=>{
                if(res.res.api_status==200) {
                    dispatch(playlistadd(res));
                }
            })
        }
    }
    function playlistadd(res) {
      return{type:PLAYLISTSONGS,message:'successfullyadded'}
    }
    function selctedSong(id){
    debugger
    return {type:PLAYLISTSELECTEDSONG,id:id}
    }
    function deletePlaylist(id){
    debugger
      return dispatch=> {
          ids = {i_playlist: id}
          ids = JSON.stringify(ids);
          Api.deletePlaylist(ids).then((res) => {
              debugger
             dispatch(playlistMessage(res.res.message))
          })
      }
    }
 function playlistMessage(message){
    return {type:PLAYLISTMESSAGE,message:message}
 }
 function deletesongPlaylist(pid,sid){
   return dispatch=> {
       id = {
           i_playlist: pid,
           i_song: sid
       }
       id = JSON.stringify(id);
       Api.deleteSong(id).then((res)=>{
           dispatch(playlistMessage(res.res.message));
       })
   }

 }
 function getGenre(){
    return dispatch=> {
        Api.getGenre().then((res)=>{
              dispatch(genreList(res));
        })
    }
 }
 function genreList(data) {
    debugger
  return {type:GENRELIST,data:data}
}
   function getGenreAlbum(id) {
    return dispatch=>{
        Api.getAlbumsGenre(id).then((res)=>{
            dispatch(genreAlbumList(res));
        })
    }
   }
   function genreAlbumList(data){
    return {type:GENREALBUMS,galbums:data}
   }


// const loginprog=()=>({type:LOGIN_INPROGRESS})
