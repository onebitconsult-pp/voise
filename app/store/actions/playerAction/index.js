import {PLAYING, PAUSED, NEXT, ERROR, PREVIOUS, CURRENT_ALBUM,CURRENT_TIME} from "../../constants";
export const  playerAction = {
    playSong,
    pauseSong,
    currentAlbum,
    currentTime
};
function currentTime(time){
    return {type:CURRENT_TIME,time:time }
}
function playSong(details){
    debugger


    return {
        type:PLAYING,isPlaying:true,songDetails:details
    }



}

function pauseSong() {
    return {
        type:PAUSED,isPlaying:false
    }

}
function currentAlbum(details) {
    return {
        type:CURRENT_ALBUM,currentALbumDetails:details
    }

}