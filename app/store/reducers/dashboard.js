import {
    GIF,LOGIN, GENRELIST,GENREALBUMS,LOGIN_INPROGRESS, LOGIN_SUC, ERROR, CURRENT_ALBUM, FORGOT, SIGNOUT, SLIDER, SEARCH,
    USERDETAILS, UPDATEPROFILE, BODY, UPLOAD, PLAYLIST,PLAYLISTSONGS,PLAYLISTMESSAGE,PLAYLISTSELECTEDSONG,USERPASSWORD,WALLET,SEARCHEMPTY
} from '../constants';
import {ToastAndroid} from "react-native";
// const initialState = {
//   baseCurrency: 'USD',
//   quoteCurrency: 'GBP',
//   amount: 100,
//   conversions: {},
// };

const initialState = {
    isLoggedIn: false,
    isLoading:true,
    FORGOT:false,
    error:undefined,
    userPlayLists:{},
    playListSongs:{},
    created:false,
    message:undefined,
    gif:false,
    genrelist:null,
    genrealbums:null


};

export default (state = initialState, action) => {

    switch (action.type) {
        case LOGIN:
            debugger
            return {...state, isLoggedIn: action.isLoggedIn,isLoading:false} ;
        case LOGIN_SUC:
            return {...state, isLoggedIn: true, uID: action.uid};
        case ERROR:
            return {...state, error:action.message};
        case LOGIN_INPROGRESS:
            return {...state,isLoading:action.isLoading};
        case CURRENT_ALBUM:
            return{...state,albumdetails:action.albumdetails};
        case FORGOT:
            return{...state,FORGOT:true,MESSAGE:action.MESSAGE};
        case SIGNOUT:
            return {...state,SIGNOUT:action.SIGNOUT,isLoggedIn:false};
        case SLIDER:
            return{...state,imgList:action.imgList,load:action.load};
        case SEARCH:
            return{...state,search:action.search,searchdata:action.data};
        case USERDETAILS:
            return {...state,details:action.details,url:undefined};
        case UPDATEPROFILE:
            return {...state, status: action.status};
        case BODY:
            return {...state, bodydetails: action.bodydetails};
        case UPLOAD:
            return {...state,url:action.url};
        case PLAYLIST:
            return{...state,userPlayLists:action.lists,created:action.created,playlisterror:action.error,message:action.message,messages:undefined}
        case PLAYLISTSONGS:
            return {...state,listsongs:action.songsdata,messages:action.message};
        case PLAYLISTMESSAGE:
            return{...state,message:action.message};
        case PLAYLISTSELECTEDSONG:
            return{...state,id:action.id};
        case USERPASSWORD:
            return{...state,passwordmessage:action.message};
        case WALLET:
            return{...state,walletDetails:action.details};
        case SEARCHEMPTY:
            return{...state,text:action.text,searchbar:action.searchbar};
        case GIF:
            return{...state,gif:action.gif};
        case GENRELIST:
            debugger
            return{...state,genrelist:action.data};
        case GENREALBUMS:
            return {...state,genrealbums:action.galbums};
        default:
            return state;
    }
};