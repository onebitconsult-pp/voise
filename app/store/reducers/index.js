import { combineReducers } from 'redux';

//import login from './login';
import dashboard from './dashboard';
import nav from './nav';
import player from './player';

export default combineReducers({
    dashboard,
    nav,
    player
});