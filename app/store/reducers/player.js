import {PLAYING, PAUSED, PREVIOUS, NEXT, CURRENT_ALBUM, CURRENT_TIME} from '../constants';
// const initialState = {
//   baseCurrency: 'USD',
//   quoteCurrency: 'GBP',
//   amount: 100,
//   conversions: {},
// };

const initialState = {
    isPlaying: false
};

export default (state = initialState, action) => {

    switch (action.type) {
        case PLAYING:
            return {...state, isPlaying: action.isPlaying,songDetails:action.songDetails} ;
        case PAUSED:
            return {...state, isPlaying: false};
        case CURRENT_ALBUM:
            return { ...state,currentPlayingAlbum:action.currentALbumDetails
            };
        case CURRENT_TIME:
            return{...state,time:action.time};
        default:
            return state;
    }
};